﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameNetworkController : MonoBehaviourPunCallbacks
{
    #region INIT INSTANCE
    private static GameNetworkController _instance;
    public static GameNetworkController Instance { get { return _instance; } }
    #endregion

    [Header("Game Settings")]
    public GameObject playerPrefabs;
    public Transform[] spawnArea;
    public bl_MiniMap minimapSystem;
    public GameObject playerListContainerParent;
    public Transform playerListContentContainer;
    public GameObject playerListPrefabs;
    private Player newPlayerJoined;
    private PhotonView gameNetworkPV;

    [Header("Connection and Network Settings")]
    public AnimateObject connectionIssuePanel;
    public TextMeshProUGUI connectionIssueText;

    [Header("Public Object Network Settings")]
    public float spawnPlaneInterval;
    public float spawnEnemyInterval;
    public float spawnVehicleInterval;

    public GameObject planeNetwork;
    public GameObject[] enemyNetwork;
    public GameObject[] vehicleNetwork;
    
    public Transform[] spawnPlaneArea;
    public Transform[] spawnEnemyArea;
    public Transform[] spawnVehicleArea;
    public Transform[] targetPlanetArea;

    private GameObject[] enemyList;
    [SerializeField] private GameObject[] vehiclesList;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PhotonNetwork.IsConnected)
            SceneChangeHandler.Instance.GoToScene(0);

        StartCoroutine(DelaySpawnPlayer());

        Debug.Log("room name : " + PhotonNetwork.CurrentRoom.Name);

        gameNetworkPV = GetComponent<PhotonView>();

        if (!PhotonNetwork.IsMasterClient)
            return;

        enemyList = new GameObject[spawnEnemyArea.Length];
        vehiclesList = new GameObject[spawnVehicleArea.Length];
        //InvokeRepeating("SpawnEnemyInterval", 1f, 2f);
        SpawnPlane();
        SpawnVehicle();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetTargetMinimap(Transform player)
    {
        minimapSystem.SetTarget(player.gameObject);
    }

    public void SpawnPlane()
    {
        InvokeRepeating("SpawnPlaneInterval", 2f, 10f);
    }

    public void SpawnVehicle()
    {
        InvokeRepeating("SpawnVehicleInterval", 2f, 10f);
    }

    public void SpawnPublicObject(GameObject objectToSpawn, Transform spawnPos)
    {
        PhotonNetwork.Instantiate(objectToSpawn.name, spawnPos.position, Quaternion.identity);
    }

    public void DestroyPublicObject(GameObject objectToDestroy)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        PhotonNetwork.Destroy(objectToDestroy);
    }

    IEnumerator DelaySpawnPlayer()
    {
        SceneChangeHandler.Instance.transitionImage.FadeOut(0f);
        yield return new WaitForSeconds(0.25f);

        int randomSpawnPoint = Random.Range(0, spawnArea.Length);
        GameObject playerSpawned = PhotonNetwork.Instantiate(
            this.playerPrefabs.name,
            new Vector3( spawnArea[randomSpawnPoint].position.x + Random.Range(0, 2), spawnArea[randomSpawnPoint].position.y, spawnArea[randomSpawnPoint].position.z + Random.Range(0, 2)),
            Quaternion.identity, 0);

        //playerSpawned.name = newPlayerJoined.NickName;
    }

    void SpawnPlaneInterval()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        gameNetworkPV.RPC("SpawnPlaneOffline", RpcTarget.AllBuffered);
    }

    void SpawnVehicleInterval()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        //gameNetworkPV.RPC("SpawnVehicles", RpcTarget.AllBuffered);
        SpawnVehicles();
    }

    void SpawnVehicles()
    {
        for (int i = 0; i < vehiclesList.Length; i++)
        {
            if (vehiclesList[i] != null)
                continue;

            if (vehiclesList[i] == null)
            {
                vehiclesList[i] = PhotonNetwork.InstantiateRoomObject(
                    vehicleNetwork[Random.Range(0, vehicleNetwork.Length)].name,
                    spawnVehicleArea[i].position,
                    spawnVehicleArea[i].rotation
                    );

                break;
            }
        }
    }

    [PunRPC]
    void SpawnPlaneOffline()
    {
        PlaneBehaviour spawnedPlane = Instantiate(planeNetwork, spawnPlaneArea[Random.Range(0, spawnPlaneArea.Length)].position, Quaternion.identity)
            .GetComponent<PlaneBehaviour>();
        //spawnedPlane.InitPlane(targetPlanetArea[Random.Range(0, targetPlanetArea.Length)].position);
        spawnedPlane.InitPlane(targetPlanetArea[Random.Range(0, targetPlanetArea.Length)]);
    }

    IEnumerator SpawnEnemyInterval()
    {
        if (!PhotonNetwork.IsMasterClient)
            yield return null;

        for (int i = 0; i < enemyList.Length; i++)
        {
            if(enemyList[i] == null)
            {
                enemyList[i] = PhotonNetwork.Instantiate(
                        enemyNetwork[Random.Range(0, enemyNetwork.Length)].name,
                        spawnPlaneArea[i].position,
                        Quaternion.identity
                    );
            }
        }

        yield return null;
    }

    IEnumerator SpawnVehiclesInterval()
    {
        if (!PhotonNetwork.IsMasterClient)
            yield return null;

        for (int i = 0; i < vehiclesList.Length; i++)
        {
            if (vehiclesList[i] == null)
            {
                vehiclesList[i] = PhotonNetwork.Instantiate(
                        vehicleNetwork[Random.Range(0, vehicleNetwork.Length)].name,
                        spawnVehicleArea[i].position,
                        Quaternion.identity
                    );
            }
        }

        yield return null;
    }

    public void ShowPlayerList()
    {
        playerListContainerParent.SetActive(!playerListContainerParent.activeSelf);
        ClearPlayerListContainer();
        UpdatePlayerList();
    }

    void UpdatePlayerList()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.IsMasterClient)
                SpawnPlayerListIndicator(player.NickName, "Master");
            else
                SpawnPlayerListIndicator(player.NickName, "Client");
        }
    }

    void ClearPlayerListContainer()
    {
        for (int i = 0; i < playerListContentContainer.childCount; i++)
        {
            Destroy(playerListContentContainer.GetChild(i).gameObject);
        }
    }

    void SpawnPlayerListIndicator(string name, string status)
    {
        PlayerListData playerListData = Instantiate(playerListPrefabs, playerListContentContainer).GetComponent<PlayerListData>();
        playerListData.InitListText(name, status);
    }


    #region RPC PHOTON CALLBACKS
    
    #endregion

    #region PHOTON CALLBACKS


    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        connectionIssueText.text = cause.ToString();
        connectionIssuePanel.FadeIn(0f);
        
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        SceneChangeHandler.Instance.GoToScene(0);
    }

    #endregion



}
