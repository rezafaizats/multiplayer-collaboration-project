﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NetworkRoomData : MonoBehaviour
{
    public TextMeshProUGUI roomText;

    public string roomName;
    public int roomSize;
    public int roomSizeMax;

    public void SetRoom(string name, int size, int sizeMax)
    {
        roomName = name;
        roomSize = size;
        roomSizeMax = sizeMax;
        roomText.text = roomName + " (" + roomSize + "/" + roomSizeMax + ")";
    }

    public void JoinRoomOnClick()
    {
        AudioController.Play("SFX_button");
        PhotonNetwork.JoinRoom(roomName);
    }

}
