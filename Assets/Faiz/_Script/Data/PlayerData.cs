﻿using System;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public PlayerID playerID;
    public PlayerAddress playerAddress;
    public PlayerGameData playerGameData;

}

[Serializable]
public class PlayerID
{
    public string playerName;
    public string playerPhone;
}

[Serializable]
public class PlayerAddress
{
    public string playerDistrict;
    public string playerSubDistrict;
}

[Serializable]
public class PlayerGameData
{
    public Vector3 playerLastLocation;
}