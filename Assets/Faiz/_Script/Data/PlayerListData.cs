﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerListData : MonoBehaviour
{
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI playerStatus;

    public void InitListText(string name, string status)
    {
        playerName.text = name;
        playerStatus.text = status;
    }

}
