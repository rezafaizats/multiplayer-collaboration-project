﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DummyEnemyNetworkController : MonoBehaviour
{
    public TextMeshPro healthText;
    public float health;
    public PhotonView enemyPV;

    // Start is called before the first frame update
    void Start()
    {
        healthText.text = health.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
            DummyNetworkController.Instance.DamagePlayer(0);
        if (Input.GetKeyDown(KeyCode.O))
            DummyNetworkController.Instance.DamagePlayer(1);
    }

    public void SetDamage(float damage)
    {
        SetHealth(damage);
        enemyPV.RPC("SetHealth", RpcTarget.Others, damage);
    }

    [PunRPC]
    public void SetHealth(float damage)
    {
        health -= damage;
        healthText.text = health.ToString();
    }

}
