﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class DummyNetworkController : MonoBehaviourPunCallbacks
{
    private static DummyNetworkController _instance;
    public static DummyNetworkController Instance { get { return _instance; } }

    [Header("Game Settings")]

    public PhotonView gmPV;

    public GameObject playerPrefabs;
    public Transform[] spawnArea;
    private Player newPlayerJoined;
    private GameObject newPlayer;
    [SerializeField] private List<GameObject> playerList;
    [SerializeField] private GameObject[] playerArray;

    public GameObject enemyPrefabs;
    public Transform spawnEnemyArea;
    [SerializeField] private List<GameObject> enemyList;
    [SerializeField] private GameObject[] enemyArray;
    private GameObject spawnedEnemy;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PhotonNetwork.IsConnected)
            SceneChangeHandler.Instance.GoToScene(0);

        //playerArray = new GameObject[10];
        //enemyArray = new GameObject[10];
        playerList = new List<GameObject>();
        enemyList = new List<GameObject>();
        StartCoroutine(DelaySpawn());

        if (!PhotonNetwork.IsMasterClient)
            return;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            SpawnEnemy();
    }

    IEnumerator DelaySpawn()
    {
        yield return new WaitForSeconds(1f);

        int randomSpawnPoint = Random.Range(0, spawnArea.Length);
        newPlayer = PhotonNetwork.Instantiate(
            this.playerPrefabs.name,
            spawnArea[randomSpawnPoint].position,
            Quaternion.identity, 0);

        //newPlayer.name = newPlayerJoined.NickName;
        gmPV.RPC("RefreshPlayerToList", RpcTarget.AllBuffered);
    }

    void SpawnEnemy()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        spawnedEnemy = PhotonNetwork.Instantiate(enemyPrefabs.name, spawnEnemyArea.position, spawnEnemyArea.rotation);
        gmPV.RPC("RefreshEnemyToList", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RefreshEnemyToList()
    {
        enemyList.Clear();
        enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemyArray)
        {
            enemyList.Add(enemy);
        }
    }

    [PunRPC]
    public void RefreshPlayerToList()
    {
        playerList.Clear();
        playerArray = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in playerArray)
        {
            playerList.Add(player);
            print(player.GetPhotonView().ViewID);
        }
    }

    public void DamageFirstEnemy()
    {
        if (enemyList[0] == null)
        {
            SpawnEnemy();
            return;
        }
        enemyList[0].GetComponent<DummyEnemyNetworkController>().SetDamage(10);
    }

    public void DamagePlayer(int index)
    {
        if (playerList[index] == null || !PhotonNetwork.IsMasterClient)
            return;

        playerList[index].GetComponent<DummyPlayerNetworkController>().SetDamage(10);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        newPlayerJoined = newPlayer;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        SceneChangeHandler.Instance.GoToScene(0);
    }
}
