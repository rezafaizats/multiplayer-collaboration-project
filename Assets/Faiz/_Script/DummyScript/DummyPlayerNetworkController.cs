﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;

public class DummyPlayerNetworkController : MonoBehaviour
{

    private static DummyPlayerNetworkController _instance;
    public static DummyPlayerNetworkController Instance { get { return _instance; } }

    public TextMeshPro healthText;
    public float health;
    public PhotonView playerPhotonView;

    private void Awake()
    {
        if (!playerPhotonView.IsMine)
            return;

        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);

        if (!playerPhotonView.IsMine)
            return;

        healthText.text = health.ToString();
        this.gameObject.name = PhotonNetwork.NickName;
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerPhotonView.IsMine)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            DummyNetworkController.Instance.DamageFirstEnemy();
        }
    }

    public void SetDamage(float damage)
    {
        SetHealth(damage);
        playerPhotonView.RPC("SetHealth", RpcTarget.Others, damage);
    }

    [PunRPC]
    public void SetHealth(float damage)
    {
        health -= damage;
        healthText.text = health.ToString();
    }

}
