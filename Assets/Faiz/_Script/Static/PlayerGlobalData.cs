﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGlobalData : MonoBehaviour
{
    #region STATIC CLASS
    private static PlayerGlobalData _instance;
    public static PlayerGlobalData Instance { get { return _instance; } }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    public PlayerData playerData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayerData(PlayerData newPlayer)    { playerData = newPlayer; }
    public PlayerData GetPlayerData() { return playerData; }


}
