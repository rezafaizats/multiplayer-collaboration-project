﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNetworkController : MonoBehaviour
{
    //private static EnemyNetworkController _instance;
    //public static EnemyNetworkController Instance { get { return _instance; } }

    [Header("Networking Enemy Scripts Options")]
    public PhotonView enemyPV;
    public CharacterState characterState;
    public EnemyBehav enemyBehaviour;

    private void Awake()
    {
        //if (!enemyPV.IsMine)
            //return;

        //if (_instance == null)
        //{
           // _instance = this;
        //}
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);

    }

    public void BrodacastDamageRPC(int damage)
    {
        if(!enemyPV.IsMine)
            return;
        enemyPV.RPC("DamageToPlayerRPC", RpcTarget.All, damage);
    }

    

    public void BrodacastPlayerDamagedRPC(CharacterState playerCS, int damage)
    {
        enemyPV.RPC("DamageToPlayerRPC", RpcTarget.All, playerCS, damage);
    }

    public void BroadcastEnemyState(GameObject target)
    {
        enemyPV.RPC("ChangeEnemyState", RpcTarget.All, target);
    }

    

    public void BroadcastSetEnemyIndex(int i)
    {
        enemyPV.RPC("SetEnemyIndex", RpcTarget.All, i);
    }
    [PunRPC]
    public void SetEnemyIndex(int i)
    {
        characterState.enemyBehav.SetIdxRPC(i);
        EnemySpawner.Instance.UpdateLogic(characterState.enemyBehav);
    }


    public void BroadcastNextPatrolTarget()
    {
        enemyPV.RPC("NextTargetPatrolRPC", RpcTarget.All);
    }
    [PunRPC]
    public void NextTargetPatrolRPC()
    {
        characterState.enemyBehav.patrol.nextTarget();
    }

    public void BroadcastSetTarget(GameObject target)
    {
        PhotonView view = target.GetComponent<PhotonView>();
        if(view==null) return;
        enemyPV.RPC("SetTargetRPC", RpcTarget.All, view.ViewID);
    }

    [PunRPC]
    public void SetTargetRPC(int target)
    {
        GameObject g = PhotonView.Find(target).gameObject;
        GetComponentInChildren<EnemyBehav>().SetTarget(g);    
    }

    [PunRPC]
    void DamagedRPC(int damage)
    {
        characterState.Damage(damage);
    }

    [PunRPC]
    void DamageToPlayerRPC(int damage)
    {
        characterState.Damage(damage);
    }

    void ChangeEnemyState(GameObject target)
    {
        enemyBehaviour.SetTarget(target);
    }

}
