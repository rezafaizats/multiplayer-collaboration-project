﻿using Photon.Pun;
using Photon.Realtime;
using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class PlayerDataRequest : MonoBehaviourPunCallbacks, ILobbyCallbacks
{
    public AnimateObject transitionImage;
    public AnimateObject mainMenuContainer;
    public AnimateObject connectingText;
    public AnimateObject welcomeButton;
    public AnimateObject signUpContainer;
    public AnimateObject loginContainer;
    public AnimateObject loggedInContainer;
    public AnimateObject errorContainer;

    [Header("Sign Up Options")]
    public TMP_InputField nameInputField;
    public TMP_InputField phoneInputField;
    public TMP_InputField districtInputField;
    public TMP_InputField subdistrictInputField;

    public PlayerGlobalData newPlayer;

    [Header("Error Handlings")]
    public TextMeshProUGUI errorInfoText;

    [Header("Data Finder Options")]
    public TMP_InputField usernameInputField;
    public TMP_InputField phoneLoginInputField;
    public TextMeshProUGUI playerNameText;

    [Header("Photon Settings")]
    public string gameVersion = "1";
    [SerializeField] private byte playerCapacity = 8;
    private bool isConnecting = false;

    [Header("Photon Lobby Settings")]
    public TMP_InputField roomNameInputField;
    public GameObject roomListingPrefab;
    public Transform roomPanel;

    [Header("Custom Setting")]
    [SerializeField] bool isCustomLoadScene;
    [SerializeField] UnityEvent customLoad;

    private void Awake()
    {

    }

    void Start()
    {
        transitionImage.FadeOut(0f);
    }

    public void StartConnecting()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = gameVersion;
        isConnecting = PhotonNetwork.ConnectUsingSettings();

        HideContainer(mainMenuContainer);
        ShowContainer(connectingText);
    }

    public void LoadPlayScene()
    {
        if (!isConnecting)
            return;

        JoinRoom();
    }

    public void StopAllProcess()
    {
        StopAllCoroutines();
        ShowContainer(mainMenuContainer);

        if (connectingText.canvasGroup.alpha == 1)
            HideContainer(connectingText);

    }

    public void ShowErrorLogin()
    {
        AudioController.Play("SFX_Error");
        ShowContainer(errorContainer);
        errorInfoText.text = "Your username / password is incorrect. Please log in again.";
    }

    public void ShowErrorLogin(string msg)
    {
        AudioController.Play("SFX_Error");
        ShowContainer(errorContainer);
        errorInfoText.text = msg;
    }

    public void PostData()
    {
        PlayerGlobalData.Instance.GetPlayerData().playerID.playerName = nameInputField.text;
        PlayerGlobalData.Instance.GetPlayerData().playerID.playerPhone = phoneInputField.text;
        PlayerGlobalData.Instance.GetPlayerData().playerAddress.playerDistrict = districtInputField.text;
        PlayerGlobalData.Instance.GetPlayerData().playerAddress.playerSubDistrict = subdistrictInputField.text;

        PostToDatabase();
    }

    public void GetData()
    {
        GetDataFromDatabase();
    }

    public void PlayButtonSFX()
    {
        if (!AudioController.DoesInstanceExist())
            return;

        AudioController.Play("SFX_button");
    }

    public void ShowContainer(AnimateObject containerToShow)
    {
        containerToShow.MoveFromCustomPosition(0.65f);
        containerToShow.FadeIn(0.65f);
    }

    public void HideContainer(AnimateObject containerToHide)
    {
        containerToHide.FadeOut(0f);
        containerToHide.MoveFromOriginalPosition(0f, new Vector3(100f, 0f, 0f));
    }

    public void JoinRoom()
    {
        PhotonNetwork.NickName = usernameInputField.text;
        RoomOptions roomOps = new RoomOptions
        {
            MaxPlayers = 5
        };
        PhotonNetwork.JoinOrCreateRoom("testRoom", roomOps, TypedLobby.Default);
    }

    public void CreateRoom()
    {
        if (roomNameInputField.text == null)
            return;

        Debug.Log("Creating room...");

        RoomOptions roomOps = new RoomOptions
        {
            MaxPlayers = 5,
            IsOpen = true,
            IsVisible = true
        };

        PhotonNetwork.JoinOrCreateRoom(roomNameInputField.text, roomOps, TypedLobby.Default);
    }

    public void JoinLobbyOnClick()
    {
        PhotonNetwork.NickName = usernameInputField.text;
        PhotonNetwork.JoinLobby();
    }

    public void RemoveRoomListing()
    {
        GameObject[] childBtn = roomPanel.GetComponentsInChildren<GameObject>();
        for (int i = 0; i < childBtn.Length; i++)
        {
            Destroy(childBtn[i]);
        }
    }

    public void ListRoom(RoomInfo roomInfo)
    {
        if (roomInfo.IsOpen && roomInfo.IsVisible)
        {
            Debug.Log("Creating room button...");
            GameObject tempListing = Instantiate(roomListingPrefab, roomPanel);
            NetworkRoomData tempButton = tempListing.GetComponent<NetworkRoomData>();
            tempButton.SetRoom(roomInfo.Name, roomInfo.PlayerCount, roomInfo.MaxPlayers);
        }
    }

    public IEnumerator DelayLoadSceneNetwork (string sceneName)
    {
        transitionImage.FadeIn(0f);
        yield return new WaitForSeconds(transitionImage.animDuration);
        PhotonNetwork.LoadLevel(sceneName);
    }

    public void LoadSceneNetwork(string sceneName)
    {
        StartCoroutine(DelayLoadSceneNetwork(sceneName));
    }

    #region FIREBASE SETTINGS

    bool SetData()
    {
        //newPlayer = new PlayerData();

        //newPlayer.playerID.playerName = nameInputField.text;
        //newPlayer.playerID.playerPhone = int.Parse(phoneInputField.text);
        //newPlayer.playerAddress.playerDistrict = districtInputField.text;
        //newPlayer.playerAddress.playerSubDistrict = subdistrictInputField.text;

        //if (newPlayer.playerID.playerName == null || newPlayer.playerID.playerPhone == 0 ||
        //    newPlayer.playerAddress.playerDistrict == null || newPlayer.playerAddress.playerSubDistrict == null)
        //    return false;

        return true;
    }

    void SetPlayerInfo(PlayerData playerInfo)
    {
        //playerStatusLogin.text = "Your Data : \n" +
        //    "Name : " + playerInfo.playerID.playerName + "\n" +
        //    "Phone : " + playerInfo.playerID.playerPhone + "\n" +
        //    "District : " + playerInfo.playerAddress.playerDistrict + "\n" +
        //    "Subdistrict : " + playerInfo.playerAddress.playerSubDistrict;
    }

    void PostToDatabase()
    {
        //PlayerGlobalData.Instance.SetPlayerData(newPlayer);
        RestClient.Put("https://unity-web-request-testing-default-rtdb.asia-southeast1.firebasedatabase.app/"
                            + PlayerGlobalData.Instance.GetPlayerData().playerID.playerName + ".json",
                            PlayerGlobalData.Instance.GetPlayerData())
            .Then(response => {
                print("done posting!");
                HideContainer(signUpContainer);
                ShowContainer(welcomeButton);
            });
    }

    void GetDataFromDatabase()
    {
        if (usernameInputField.text == null || phoneLoginInputField.text == null)
            return;

        RestClient.Get<PlayerData>("https://unity-web-request-testing-default-rtdb.asia-southeast1.firebasedatabase.app/"
                                        + usernameInputField.text + ".json")
            .Then(response => {

                if (phoneLoginInputField.text != response.playerID.playerPhone.ToString())
                {
                    //Login info are missing
                    ShowErrorLogin("Please input your username / password into the correct field.");
                    return;
                }
                PlayerGlobalData.Instance.SetPlayerData(response);

                //Login success
                HideContainer(loginContainer);
                ShowContainer(loggedInContainer);
                playerNameText.text = "Welcome, " + PlayerGlobalData.Instance.GetPlayerData().playerID.playerName + "!";

            })
            .Catch(e => {
                //Login erro
                ShowErrorLogin("Username / password are incorrect.");
            });
    }

    #endregion

    #region PHOTON SETTINGS

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        JoinRoom();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        connectingText.FadeOut(0f);
        connectingText.MoveFromOriginalPosition(0f);
        welcomeButton.FadeIn(connectingText.animDuration);
        connectingText.MoveFromCustomPosition(connectingText.animDuration);
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room has been created");
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        PhotonNetwork.NickName = usernameInputField.text;
        if (isCustomLoadScene)
        {
            customLoad.Invoke();
        }
        else
        {
            Debug.Log("Joining room... " + PhotonNetwork.CurrentRoom.Name);
            //transitionImage.FadeIn(0f);
            //PhotonNetwork.LoadLevel("GameTesting_Scene");
            StartCoroutine(DelayLoadSceneNetwork("GameTesting_Scene"));
        }
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Joined lobby");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        errorInfoText.text = cause.ToString();
        ShowContainer(errorContainer);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log("Room List Updated");
        foreach (RoomInfo room in roomList)
        {
            ListRoom(room);
        }
    }

    #endregion

}
