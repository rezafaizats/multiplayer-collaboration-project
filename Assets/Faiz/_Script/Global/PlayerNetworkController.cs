﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworkController : MonoBehaviourPunCallbacks, IPunObservable
{
    private static PlayerNetworkController _instance;
    public static PlayerNetworkController Instance { get { return _instance; } }

    [Header("Networking Player Scripts Options")]
    public Player_Input localPlayerInput;
    public PhotonView playerPhotonView;
    public CharacterState characterStatus;

    private void Awake()
    {
        if (!playerPhotonView.IsMine)
            return;

        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);

        if (!playerPhotonView.IsMine)
        {
            localPlayerInput.isAuthorized = false;
            return;
        }
        
        localPlayerInput.isAuthorized = true;
        GameNetworkController.Instance.SetTargetMinimap(this.transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerPhotonView.IsMine)
            return;

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            //GameNetworkController.Instance.ShowPlayerList();
        }

    }

    public void BroadcastDamageRPC(int damage)
    {
        if (!playerPhotonView.IsMine)
            return;

        playerPhotonView.RPC("DamagedRPC", RpcTarget.All, damage);
    }

    [PunRPC]
    void DamagedRPC(int damage)
    {
        characterStatus.Damage(damage);
    }

    #region PHOTON CALLBACKS

    public void DisconnectFromGame()
    {
        GameNetworkController.Instance.LeaveRoom();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(this.characterStatus.health);
        }
        else
        {
            this.characterStatus.health = (int)stream.ReceiveNext();
        }
    }

    #endregion
}
