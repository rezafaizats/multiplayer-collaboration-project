﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class LobbyNetworkHandler : MonoBehaviour, ILobbyCallbacks
{
    public GameObject roomListingPrefab;
    public Transform roomPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void RemoveRoomListing()
    {
        while (roomPanel.childCount != 0)
        {
            Destroy(roomPanel.GetChild(0).gameObject);
        }
    }

    void ListRoom(RoomInfo roomInfo)
    {
        if(roomInfo.IsOpen && roomInfo.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomPanel);
            NetworkRoomData tempButton = tempListing.GetComponent<NetworkRoomData>();
            tempButton.SetRoom(roomInfo.Name, roomInfo.PlayerCount, roomInfo.MaxPlayers);
        }
    }

    public void JoinLobbyOnClick()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    public void OnJoinedLobby()
    {

    }

    public void OnLeftLobby()
    {

    }

    public void OnLobbyStatisticsUpdate(List<TypedLobbyInfo> lobbyStatistics)
    {

    }

    public void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo room in roomList)
        {
            ListRoom(room);
        }
    }
}
