﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AnimateObject : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public Vector3 moveFrom;
    public Vector3 moveTo;
    public float animDuration;
    public LeanTweenType animEasing;

    public void FadeIn(float delayAnim)
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;

        LeanTween.alphaCanvas(canvasGroup, 1f, animDuration)
            .setDelay(delayAnim)
            .setEase(animEasing)
            .setOnComplete(() => SetInteractableRaycast(true));
    }

    public void FadeIn(float delayAnim, string audioToPlay)
    {
        canvasGroup.interactable = false;
        canvasGroup.alpha = 0;

        LeanTween.alphaCanvas(canvasGroup, 1f, animDuration)
            .setDelay(delayAnim)
            .setEase(animEasing)
            .setOnComplete(() => SetInteractableRaycast(true));

        if (string.IsNullOrEmpty(audioToPlay))
            return;

        AudioController.Play(audioToPlay);
    }

    public void FadeOut(float delayAnim)
    {
        canvasGroup.interactable = true;
        canvasGroup.alpha = 1f;

        LeanTween.alphaCanvas(canvasGroup, 0f, animDuration)
            .setDelay(delayAnim)
            .setEase(animEasing)
            .setOnComplete(() => SetInteractableRaycast(false));
    }

    public void FadeOut(float delayAnim, string audioToPlay)
    {
        canvasGroup.alpha = 1f;

        LeanTween.alphaCanvas(canvasGroup, 0f, animDuration)
            .setDelay(delayAnim)
            .setEase(animEasing);

        if (string.IsNullOrEmpty(audioToPlay))
            return;

        AudioController.Play(audioToPlay);
    }

    public void MoveFromOriginalPosition(float animDelay)
    {
        LeanTween.moveLocal(canvasGroup.gameObject, moveTo, animDuration)
            .setDelay(animDelay)
            .setEase(animEasing);
    }

    public void MoveFromOriginalPosition(float animDelay, Vector3 customTargetPos)
    {
        LeanTween.moveLocal(canvasGroup.gameObject, customTargetPos, animDuration)
            .setDelay(animDelay)
            .setEase(animEasing);
    }

    public void MoveFromCustomPosition(float animDelay)
    {
        canvasGroup.transform.localPosition = moveFrom;
        LeanTween.moveLocal(canvasGroup.gameObject, moveTo, animDuration)
            .setDelay(animDelay)
            .setEase(animEasing);
    }

    void SetInteractableRaycast(bool set)
    {
        canvasGroup.interactable = set;
        canvasGroup.blocksRaycasts = set;
    }

}
