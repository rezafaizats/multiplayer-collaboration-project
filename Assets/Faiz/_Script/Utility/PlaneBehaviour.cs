﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneBehaviour : MonoBehaviour
{
    public float planeSpeed;
    public Vector3 planeDestination;

    private bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if(isMoving)
            MovePlane();
    }

    public void InitPlane(Transform target)
    {
        Vector3 addition = target.right*Random.Range(0f, 15f);
        planeDestination = target.position+addition;
        //planeDestination = new Vector3((target.position.x + Random.Range(0f, 15f)), target.position.y, target.position.z + Random.Range(0f, 15f));
        transform.LookAt(planeDestination);
        isMoving = true;
    }

    public void MovePlane()
    {
        transform.LookAt(planeDestination);
        transform.position += transform.forward * planeSpeed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this);
    }

}
