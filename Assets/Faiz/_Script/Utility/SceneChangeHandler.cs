﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeHandler : MonoBehaviour
{
    #region INIT INSTANCE

    private static SceneChangeHandler _instance;
    public static SceneChangeHandler Instance { get { return _instance; } }

    #endregion

    [Header("Scene Settings")]
    public string[] sceneIndexList;

    [Header("Animation Options")]
    public AnimateObject transitionImage;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    public void GoToScene(int index)
    {
        transitionImage.FadeIn(0f);
        StartCoroutine(ChangeSceneDelay(sceneIndexList[index], transitionImage.animDuration));
    }

    IEnumerator ChangeSceneDelay(string indexScene, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(indexScene);
    }

    public void QuitApps()
    {
        Application.Quit();
    }

}
