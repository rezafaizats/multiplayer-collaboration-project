﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PublicObjectController : MonoBehaviour
{
    public bool isMoving;
    public float speed;
    private Transform targetDestination;

    private PhotonView objectPV;

    // Start is called before the first frame update
    void Start()
    {
        objectPV = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetDestination == null)
            return;

        float distance = Vector3.Distance(transform.position, targetDestination.position);
        if (distance <= 1f)
            GameNetworkController.Instance.DestroyPublicObject(this.gameObject);
    }

    private void FixedUpdate()
    {
        if (targetDestination == null)
            return;

        MoveObject();
    }

    public void MoveObject()
    {
        transform.Translate(-transform.forward * speed * Time.deltaTime);
    }

    public void BroadcastInitPlane(Transform targetPos)
    {
        objectPV.RPC("InitObject", RpcTarget.All, targetPos);
    }

    [PunRPC]
    public void InitObject(Transform targetPos)
    {
        targetDestination = targetPos;
        transform.LookAt(targetDestination);
    }

}
