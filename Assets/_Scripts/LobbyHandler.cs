﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class LobbyHandler : MonoBehaviourPunCallbacks
{
    [Header("For UI Integration")]
    public GameObject panelLobby;
    public GameObject panelRoom;
    public Text[] player;

    [Header("Insert multiplayer scene name here!!")]
    public string sceneName;

    string roomName;
    bool isConnecting;

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void CreateRoomClick(string _roomName)
    {
        roomName = _roomName;
        isConnecting = true;

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("Joining Room...");

            JoinRoom();
        }
        else
        {
            Debug.Log("Connecting...");

            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = "1";
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    void JoinRoom()
    {
        RoomOptions roomOps = new RoomOptions
        {
            MaxPlayers = 5
        };
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOps, TypedLobby.Default);

        Debug.Log("your name:" + PhotonNetwork.NickName);
    }

    [PunRPC]
    void SetPlayerName()
    {
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            player[i].text = PhotonNetwork.PlayerList[i].NickName;
        }
    }

    public void StartGame()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount < 4)
        {
            Debug.Log("not enuf players!");
        }
        else
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
    }

    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            Debug.Log("OnConnectedToMaster: Next -> try to Join a Room");
            // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
            JoinRoom();
        }
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("room created!" + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("room failed to be created!");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("your room:" + PhotonNetwork.CurrentRoom.Name);

        panelLobby.SetActive(false);
        panelRoom.SetActive(true);

        SetPlayerName();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("<Color=Red>OnJoinRandomFailed</Color>: Next -> Create a new Room");

        // #Critical: we failed to join a room, maybe none exists or they are all full. No worries, we create a new room.
        JoinRoom();
    }

    public override void OnPlayerEnteredRoom(Player otherPlayer)
    {
        Debug.Log("this guy entered! " + otherPlayer.NickName);

        SetPlayerName();
        //photonView.RPC("SetPlayerName", RpcTarget.All);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log("this guy left! " + otherPlayer.NickName);

        SetPlayerName();
        //photonView.RPC("SetPlayerName", RpcTarget.All);
    }

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom() called by PUN");

        isConnecting = false;
        panelLobby.SetActive(true);
        panelRoom.SetActive(false);
    }

    #endregion
}