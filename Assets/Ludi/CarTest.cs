﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarTest : MonoBehaviour
{
    public GameObject player;
    public GameObject mainCamera;
    public GameObject fakeCar;
    public GameObject realCar;
    public Transform fakeCarPos;
    public Transform realCarPos;
    public Transform playerPos;
    public bool isDrive;
    // Start is called before the first frame update
    void Start()
    {
        isDrive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            if(isDrive == false)
            {
                mainCamera.SetActive(false);
                player.SetActive(false);
                fakeCar.SetActive(false);
                realCar.SetActive(true);
                isDrive = true;
            }
            else
            {
                mainCamera.SetActive(true);
                player.SetActive(true);
                realCar.SetActive(false);
                fakeCar.SetActive(true);
                isDrive = false;
            }
            fakeCarPos.position = realCarPos.position;
            playerPos.position = fakeCarPos.position;

        }
    }
}
