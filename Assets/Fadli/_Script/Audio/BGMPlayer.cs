﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMPlayer : MonoBehaviour
{
    public static BGMPlayer Instance;
    public string BGMName;
    public bool playOnStart = true;

    public float fadeTime = 1;

    void Awake()
    {
         if (Instance == null) 
        {
            Instance = this;
        }else
        {
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        if(playOnStart)
        {
            AudioController.PlayMusic(BGMName);
        }
    }
    public void play()
    {
        AudioController.PlayMusic(BGMName);
    }
    public void fadeOut()
    {
        Game_SoundConfig.Instance.fadeOutBGMMusic(fadeTime);
    }
    public void stop()
    {
        AudioController.StopMusic();
    }
    // Update is called once per frame
    
}
