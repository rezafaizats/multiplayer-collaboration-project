﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioButtonSFX : MonoBehaviour
{
   [SerializeField] string buttonSFXName = "Button";
   [SerializeField] bool isCustomPlacement = false;
   [SerializeField] Transform customPlacement;

   public void PlaySound()
   {
       if(isCustomPlacement)
       {
           AudioController.Play(buttonSFXName, customPlacement);
       }
       else
       {
           AudioController.Play(buttonSFXName, Camera.main.transform);
       }
       
   } 
}
