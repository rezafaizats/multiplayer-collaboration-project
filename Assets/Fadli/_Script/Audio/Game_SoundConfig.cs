﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Game_SoundConfig : MonoBehaviour
{
    
    public static Game_SoundConfig Instance;

    [SerializeField] GameObject AudioControllerPrefab;

    public AudioMixer audioMixer;

    [SerializeField] bool useIndicator = true;
    [SerializeField] Text IndicatorSFX;
    [SerializeField] Text IndicatorBGM;

    //[SerializeField] MSSceneControllerFree control;
    [Range(0.000001f, 1)]
    [SerializeField] float BGM_Volume;
    //[SerializeField] bool BGM_Muted;
    [Range(0.000001f, 1)]
    [SerializeField] float SFX_Volume;
    //[SerializeField] bool SFX_Muted;
    [SerializeField] bool updateVolume=false;

    [SerializeField] bool destroyWhenLoadScene = false;

    void Update()
    {
        if(updateVolume)
        {
            setVolumeBGM(BGM_Volume, true);
            setVolumeSFX(SFX_Volume, true);
            updateVolume = false;
        }
    }
    public void fadeOutBGMMusic(float timeToFade)
    {
        AudioController.FadeOutCategory("BGM", timeToFade);
    }
    void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;
            if(!AudioController.DoesInstanceExist())
            {
                Instantiate(AudioControllerPrefab, Vector3.zero, Quaternion.identity, this.gameObject.transform);
            }
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
            if(!destroyWhenLoadScene)
                DontDestroyOnLoad(this.gameObject);
        }
        else Destroy(gameObject);
    }
    void Start()
    {
        if(useIndicator){
            IndicatorSFX = GameObject.Find("IndicatorSFX").GetComponent<Text>();
            IndicatorBGM = GameObject.Find("IndicatorBGM").GetComponent<Text>();
        }
        //control = GameObject.Find("Control").GetComponent<MSSceneControllerFree>();



        //AudioController.PlayMusic("Game_Music", Camera.main.transform.position, Camera.main.transform,1,0,0 );
        if(PlayerPrefs.HasKey("BGM"))
        {
            setVolumeBGM(PlayerPrefs.GetFloat("BGM"));
        }else setVolumeBGM(AudioController.GetCategoryVolume("BGM"));

        if(PlayerPrefs.HasKey("SFX"))
        {
            setVolumeSFX(PlayerPrefs.GetFloat("SFX"));
        }else setVolumeSFX(AudioController.GetCategoryVolume("SFX"));

        if(useIndicator)
        {
            if(isMutedBGM()) IndicatorBGM.text = "BGM OFF";
            else IndicatorBGM.text = "BGM ON";

            if(isMutedSFX()) IndicatorSFX.text = "SFX OFF";
            else IndicatorSFX.text = "SFX ON";
        }
        
        
       
    }
    void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode mode)
    {
        
    }
    public void muteBGM(bool mute)
    {
        if(mute)
        {
            setVolumeBGM(0.000001f);
            //audioMixer.SetFloat("BGM", Mathf.Log10(0.000001f)*30);
            //AudioController.SetCategoryVolume("BGM", 0);
            //AudioController.StopMusic();
            if(useIndicator)
                IndicatorBGM.text="BGM OFF";
        }else
        {
            resetVolumeBGM();
            //audioMixer.SetFloat("BGM", Mathf.Log10(BGM_Volume)*30);
            //AudioController.SetCategoryVolume("BGM", BGM_Volume);
            //AudioController.PlayMusic("Game_Music", Camera.main.transform.position, Camera.main.transform,1,0,0 );
            if(useIndicator)
                IndicatorBGM.text="BGM ON";
        }  
    }
    public void muteSFX(bool mute)
    {
        if(mute)
        {
            setVolumeSFX(0.000001f);
            //audioMixer.SetFloat("SFX", Mathf.Log10(0.000001f)*30);
            //AudioController.SetCategoryVolume("SFX", 0);
            //muteAllVehicleSFX();
            if(useIndicator)
                IndicatorSFX.text="SFX OFF";
            
        }else
        {
            resetVolumeSFX();
            //audioMixer.SetFloat("SFX", Mathf.Log10(BGM_Volume)*30);
            //AudioController.SetCategoryVolume("SFX", BGM_Volume);
            //unmutedAllVehiclesSFX();
            if(useIndicator)
                IndicatorSFX.text="SFX ON";
        }  

    }

///    
/*
    void muteAllVehicleSFX()
    {
        foreach(GameObject g in control.vehicles)
        {
            AudioSource [] audios = g.GetComponentsInChildren<AudioSource>();
            foreach(AudioSource a in audios)
            {
                a.enabled = false;
            }
        }
    }
    void unmutedAllVehiclesSFX()
    {
        foreach(GameObject g in control.vehicles)
        {
            AudioSource [] audios = g.GetComponentsInChildren<AudioSource>();
            foreach(AudioSource a in audios)
            {
                a.enabled = true;
            }
        }
    }

    */
    //<summary>
    //Set BGM Volume Setting
    //</summary>
    public void setVolumeBGM(float volume, bool save = false)
    {
        if(save) PlayerPrefs.SetFloat("BGM", volume);
        AudioController.SetCategoryVolume("BGM", volume);
        audioMixer.SetFloat("BGM", Mathf.Log10(volume)*30);
        BGM_Volume = volume;
    }
    //<summary>
    //Set SFX Volume Setting
    //</summary>
    public void setVolumeSFX(float volume, bool save = false)
    {
        if(save) PlayerPrefs.SetFloat("SFX", volume);
        AudioController.SetCategoryVolume("SFX", volume);
        audioMixer.SetFloat("SFX", Mathf.Log10(volume)*30);
        SFX_Volume = volume;
    }

    //<summary>
    //Reset BGM volume to BGM Volume Setting
    //</summary>
    public void resetVolumeBGM()
    {
        if(PlayerPrefs.HasKey("BGM"))
        {
            setVolumeBGM(PlayerPrefs.GetFloat("BGM"));
        }else setVolumeBGM(1, true);
    }
    public void resetVolumeSFX()
    {
        if(PlayerPrefs.HasKey("SFX"))
        {
            setVolumeSFX(PlayerPrefs.GetFloat("SFX"));
        }else setVolumeSFX(1, true);
        
    }

    public bool isMutedBGM()
    {
        if(AudioController.GetCategoryVolume("BGM") > 0.000001f)
        //float vol;
        //audioMixer.GetFloat("BGM", out vol);
        //if(vol>Mathf.Log10(0)*30)
        {
            return false;
        }
        else return true;
    }
    public bool isMutedSFX()
    {
        if(AudioController.GetCategoryVolume("SFX") > 0.000001f)
        //float vol;
        //audioMixer.GetFloat("SFX", out vol);
        //if(vol>Mathf.Log10(0)*30)
        {
            return false;
        }
        else return true;
    }

}
