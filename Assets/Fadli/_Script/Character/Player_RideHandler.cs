﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class Player_RideHandler : MonoBehaviour
{
    [SerializeField] CharacterState state;
    /*
    [SerializeField] GameObject controlPrefab;
    
    [SerializeField] MSSceneControllerFree control;
    //*/
    public MSVehicleControllerFree vehicleCode;
    public VehicleHopper hopper;
    public bool isInsideTheCar = false;
    [SerializeField] UnityEvent EnterCarEvent;
    [SerializeField] UnityEvent ExitCarEvent;
    
    public void SetControl()
    {
        //Using Control
        /*
        GameObject g = GameObject.Find("Control");
        if(g==null)
        {
            g = Instantiate(controlPrefab, Vector3.zero, Quaternion.identity);
            g.name = "Control";
            control = g.GetComponent<MSSceneControllerFree>();
        }else
        {
            control = g.GetComponent<MSSceneControllerFree>();
        }
        control.player = state.gameObject;
        control.vehicles = GameObject.FindGameObjectsWithTag("Vehicle");
        foreach(GameObject veh in control.vehicles)
        {
            //veh.GetComponent<VehicleOwnership>();
        }
        control.Init();
        //*/
    }

    
    public bool ExitCarCheck()
    {
        if(vehicleCode!=null)
        {

            vehicleCode.ExitTheVehicle();
            state.GetComponent<Collider>().enabled = true;
            state.GetComponent<Collider>().isTrigger = false;
            state.GetComponent<Rigidbody>().isKinematic = false;
            state.transform.SetParent(null);
            state.transform.position = vehicleCode.doorPosition[0].transform.position;
            state.charMesh.SetActive(true);
            state.equipment.switchWeapon(state.anim.GetBool("IsRange"));
            state.GetComponentInChildren<Player_Input>().cameraWork.showCamera();
            //state.GetComponentInChildren<Player_Input>().setControlable(true);
            vehicleCode.seat.isUsed = false;
            isInsideTheCar=false;
            return true;
        }else return false;

        #region  OldEnterCar
        /*
        if(vehicleCode!=null)
        {

            vehicleCode.ExitTheVehicle();
            CharacterState state = control.player.GetComponent<CharacterState>();
            control.player.GetComponent<Collider>().enabled = true;
            control.player.transform.SetParent(null);
            control.player.transform.position = vehicleCode.doorPosition[0].transform.position;
            state.GetComponent<Rigidbody>().isKinematic = false;
            state.GetComponent<Collider>().isTrigger = false;
            control.player.GetComponent<CharacterState>().charMesh.SetActive(true);
            state.equipment.switchWeapon(state.anim.GetBool("IsRange"));
            state.GetComponentInChildren<Player_Input>().cameraWork.showCamera();
            //state.GetComponentInChildren<Player_Input>().setControlable(true);

            
            vehicleCode.seat.isUsed = false;
            isInsideTheCar=false;
            vehicleCode=null;
            return true;
        }
        return false;
        ///*/
        #endregion
    }
    public void EnterCar()
    {
        if(!isWaitForDelay)
        {
            StartCoroutine(waitToEnter());
        }
    }
    public void ExitCar()
    {
        if(ExitCarCheck())
        {
            if(ExitCarEvent!=null) ExitCarEvent.Invoke();
        }
        //if(!isWaitForDelay)
        //{
        //    StartCoroutine(waitToExit());
        //}
    }

    bool isWaitForDelay = false;
    IEnumerator waitToEnter()
    {
        isWaitForDelay = true;
        yield return new WaitForSeconds(0.01f);
        if(EnterCarCheck())
        {
            if(EnterCarEvent!=null)
            {
                EnterCarEvent.Invoke();
            }
        }
        isWaitForDelay = false;   
    }
    IEnumerator waitToExit()
    {
        isWaitForDelay = true;
        yield return new WaitForSeconds(0.01f);
        ExitCarCheck();
        isWaitForDelay = false;   
    }

    bool findVehicle()
    {
        return false;


        #region  Old
        /*
        foreach(GameObject veh in control.vehicles)
        {
            MSVehicleControllerFree t = veh.GetComponent<MSVehicleControllerFree>();
            if(t.isInsideTheCar)
            {
               vehicleCode = t;
               return true;
            }
        }
        return false;
        ///*/
        #endregion
    }
    public bool EnterCarCheck()
    {
        if(vehicleCode==null)
        {
            return false;
        }
        if(vehicleCode.seat.isUsed)
        {
            vehicleCode = null;
            return false;
        }
        vehicleCode.EnterInVehicle();
        state.GetComponentInChildren<Player_Input>().cameraWork.hideAllCamera();
        state.GetComponent<Rigidbody>().isKinematic = true;
        state.GetComponent<Collider>().isTrigger = true;
        state.charMesh.SetActive(false);
        state.equipment.unarm();

        state.transform.position = vehicleCode.seat.position.position;
        state.transform.SetParent(vehicleCode.seat.position);
        vehicleCode.seat.isUsed =true;
        isInsideTheCar = true;
        return true;

        #region Old
        /*
        if(control!=null)
        {
            vehicleCode = control.getVehicleCode();
            if(vehicleCode==null)
            {
                return false;
            }
            if(!vehicleCode.isInsideTheCar)
            {
                if(!findVehicle())
                {
                    return false;
                }
            }
            if(vehicleCode.seat.isUsed)
            {
                return false;
            }
            CharacterState state = control.player.GetComponent<CharacterState>();
            state.GetComponent<Rigidbody>().isKinematic = true;
            state.GetComponent<Collider>().isTrigger = true;
            state.charMesh.SetActive(false);
            state.equipment.unarm();
            state.GetComponentInChildren<Player_Input>().cameraWork.hideAllCamera();
            //state.GetComponentInChildren<Player_Input>().setControlable(false);
            

            control.player.transform.position = vehicleCode.seat.position.position;
            control.player.transform.SetParent(vehicleCode.seat.position);
            vehicleCode.seat.isUsed =true;
            isInsideTheCar = true;
            return true;
        }
        return false;
        ///*/
        #endregion
    }

    

    public void ExitCarNetwork()
    {
        //state.GetComponent<Collider>().enabled = true;
        //state.GetComponent<Collider>().isTrigger = false;
        //state.GetComponent<Rigidbody>().isKinematic = false;
        //state.charMesh.SetActive(true);

        GetComponent<PhotonView>().RPC("BodyVisibility", RpcTarget.All, true);

        //state.transform.SetParent(null);
        isInsideTheCar=false;
        state.transform.position = hopper.vehicleScript.doorPosition[0].transform.position;
        //state.equipment.switchWeapon(state.anim.GetBool("IsRange"));
        hopper.vehicleScript.ExitTheVehicle();
        state.GetComponentInChildren<Player_Input>().showCam();
        hopper.vehicleScript.seat.isUsed = false;
        
    }

    [PunRPC]
    public void BodyVisibility(bool val)
    {
        state.GetComponent<Collider>().enabled = val;
        state.GetComponent<Collider>().isTrigger = !val;
        state.GetComponent<Rigidbody>().isKinematic = !val;
        state.GetComponentInChildren<CharacterMovement>().isUseGravity = val;
        state.charMesh.SetActive(val);
        if(val)
        {
            state.equipment.switchWeapon(state.anim.GetBool("IsRange"));
        }else
        {
            state.equipment.unarm();
        }
    }

    public void EnterCarNetwork()
    {
        hopper.vehicleScript.EnterInVehicle();
        state.GetComponentInChildren<Player_Input>().hideCam();

        //state.GetComponent<Rigidbody>().isKinematic = true;
        //state.GetComponent<Collider>().isTrigger = true;
        //state.charMesh.SetActive(false);

        GetComponent<PhotonView>().RPC("BodyVisibility", RpcTarget.All, false);

        //state.equipment.unarm();
        //state.transform.position = hopper.vehicleScript.seat.position.position;
        //state.transform.SetParent(hopper.vehicleScript.seat.position);
        hopper.vehicleScript.seat.isUsed =true;
        isInsideTheCar = true;
    }
    // Update is called once per frame
    void Update()
    {
        if(isInsideTheCar)
        {
            if(hopper.isUsedInNetwork)
            {
                if(state.GetComponent<Photon.Pun.PhotonView>().IsMine)
                {
                    state.transform.position = hopper.vehicleScript.seat.position.position;
                }
            }
            else
            {
                state.transform.position = hopper.vehicleScript.seat.position.position;
            }
            
        }
    }
}
