﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class CharacterState : MonoBehaviour
{
    public Animator anim;
    public GameObject charMesh;
    public CharacterMovement characterMovement;
    public Character_EquipmentHandler equipment;
    public Rigidbody rigidb;
    public FieldOfView fieldOfView; 
    public GameObject target;

    [Header("EnemySetting")]
    public EnemyBehav enemyBehav;

    public EnemyNetworkController enemyNetworkController;
    

    [Header("Status")]
    public int health;
    public bool insideVehicle;

    [SerializeField] public bool isDeath = false;

    [Header("Preference")]
    [SerializeField] bool destroyWhenDead = true;
    [SerializeField] float destroyDelay = 5;


    [SerializeField] UnityEvent hitEvent;
    [SerializeField] UnityEvent deathEvent;
    public bool isUseNetwork = false;

    
    public void Damage(int damage)
    {

        #region  RPCLama
        /*
        if(isUseNetwork)
        {
            DamageNetwork(damage);
            GetComponent<PhotonView>().RPC("DamageNetwork", RpcTarget.Others, damage);
        }
        else
        {
            health-=damage;
            anim.SetInteger("Random", Random.Range(1,3));
            anim.SetTrigger("Damage");
            if(hitEvent!=null)
            {
                hitEvent.Invoke();
            }
        }
        */
        #endregion

        health-=damage;
        anim.SetInteger("Random", Random.Range(1,3));
        anim.SetTrigger("Damage");
        if(hitEvent!=null)
        {
            hitEvent.Invoke();
        }
    }

    [PunRPC]
    public void DamageNetwork(int damage)
    {
        health-=damage;
        anim.SetInteger("Random", Random.Range(1,3));
        anim.SetTrigger("Damage");
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    void Update()
    {
        
        if(!isDeath && health<=0)
        {
            anim.Play("Death");
            anim.SetBool("Dead", true);
            isDeath = true;
            GetComponent<Collider>().enabled = false;
            if(deathEvent!=null)
            {
                if(GetComponent<PhotonView>().IsMine)
                {
                    deathEvent.Invoke();
                }
            }
            if(destroyWhenDead)
                Destroy(this.gameObject, destroyDelay);
        }
    }

}
