using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_NameDisplay : MonoBehaviour
{
    [SerializeField] GameObject NameDisplayUIPrefab;
    [SerializeField] Transform displayLocTarget; 
    [SerializeField] GameObject instantiatedNameDispaly;
    [SerializeField] Slider healthSlider;

    [SerializeField] bool showName = true;
    bool isCopyLoc;

    //[Header("Network")]
    //[SerializeField] bool isUseNameInNetwork = false;

    void Start()
    {
        
    }
    

    void OnEnable()
    {
        if(instantiatedNameDispaly==null)
        {
            GameObject g = GameObject.Find("Canvas");
            GameObject i = Instantiate(NameDisplayUIPrefab);
            i.transform.SetParent(g.transform);
            i.name = "Name UI "+this.gameObject.name;
            string name;
            if(showName)
            {
                if(GetComponent<CharacterState>().isUseNetwork)
                {
                    name = GetComponent<Photon.Pun.PhotonView>().Owner.NickName;
                    if(string.IsNullOrWhiteSpace(name))
                    {
                        name = "player id "+GetComponent<Photon.Pun.PhotonView>().Owner.ActorNumber.ToString();
                    }
                }else
                {
                    name = this.gameObject.name;
                }
            }else
            {
                 name= "";
            }
            i.GetComponent<Text>().text = name;
            healthSlider = i.GetComponentInChildren<Slider>();
            instantiatedNameDispaly = i;
            isCopyLoc = true;
        }
    }

    void OnDisable()
    {
        if(instantiatedNameDispaly!=null)
        {
            isCopyLoc = false;
            Destroy(instantiatedNameDispaly);
            instantiatedNameDispaly = null;
        }
    }

    void placingUI()
    {
        Vector3 vectorToTarget = (displayLocTarget.position-Camera.main.transform.position).normalized;
            if(Vector3.Angle(Camera.main.transform.forward, vectorToTarget)< 90)
            {
                if(!instantiatedNameDispaly.activeSelf)
                {
                    instantiatedNameDispaly.SetActive(true);
                }
                instantiatedNameDispaly.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, displayLocTarget.position);

            }
            else
            {
                if(instantiatedNameDispaly.activeSelf)
                {
                    instantiatedNameDispaly.SetActive(false);
                }
            }
            //this.transform.position = Camera.main.WorldToScreenPoint(targetPos.transform.position);
        }
    
    void Update()
    {
        if(instantiatedNameDispaly!=null)
        {
            if(isCopyLoc) 
            {
                placingUI();
            }
        }
        if(healthSlider!=null)
        {
            int health = GetComponent<CharacterState>().health;
            healthSlider.value = (float) health/100;
            if(health<0)
            {
                this.OnDisable();
                this.enabled = false;
            }
        }
    }
}
