﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class Player_CameraWork : MonoBehaviour
{
    [SerializeField] GameObject state;
    [SerializeField] CinemachineBrain cinemachineBrain;
    [SerializeField] GameObject camThirdPerson;
    [SerializeField] GameObject camFirstPerson;
    [SerializeField] GameObject cameraLookAt;

    [SerializeField] UnityEvent OnFPCam;
    [SerializeField] UnityEvent OnTPCam;


    public bool isUseThirdPerson = true;

    public void StopControlCamera()
    {
        isCameraCtrlable = false;
        Cinemachine.CinemachineCore.GetInputAxis = GetAxisCustom;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void StartControlCamera()
    {
        isCameraCtrlable = true;
        Cinemachine.CinemachineCore.GetInputAxis = GetAxisCustom;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    bool isCameraCtrlable = true;
    public float GetAxisCustom(string axisName){
        if(axisName == "Mouse X"){
            if (isCameraCtrlable){
                return UnityEngine.Input.GetAxis("Mouse X");
            } else{
                return 0;
            }
        }
        else if (axisName == "Mouse Y"){
            if (isCameraCtrlable){
                return UnityEngine.Input.GetAxis("Mouse Y");
            } else{
                return 0;
            }
        }
        return UnityEngine.Input.GetAxis(axisName);
    }
    public void assignCamera()
    {
        cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();

        camThirdPerson = GameObject.Find("ThirdPerson");
        CinemachineFreeLook tp =camThirdPerson.GetComponent<CinemachineFreeLook>();
        tp.Follow = state.transform;
        tp.LookAt = cameraLookAt.transform;


        camFirstPerson = GameObject.Find("FirstPerson");
        CinemachineVirtualCamera fp = camFirstPerson.GetComponent<CinemachineVirtualCamera>();
        fp.Follow = cameraLookAt.transform;

        camFirstPerson.SetActive(false);
        camThirdPerson.SetActive(true);
        isUseThirdPerson = true;
    }

    public void setActiveCineBrain(bool val)
    {
        cinemachineBrain.gameObject.SetActive(val);
    }
    public void hideAllCamera()
    {
        camFirstPerson.SetActive(false);
        camThirdPerson.SetActive(false);
    }
    public void showCamera()
    {
        isUseThirdPerson=!isUseThirdPerson;
        ChangeCamera();
    }
    public void ChangeCamera()
    {
        if(isUseThirdPerson)
        {
            camThirdPerson.SetActive(false);
            camFirstPerson.SetActive(true);
            isUseThirdPerson = false;
            OnFPCam.Invoke();
        }
        else
        {
            camThirdPerson.SetActive(true);
            camFirstPerson.SetActive(false);
            isUseThirdPerson = true;
            OnTPCam.Invoke();
        }
    }


}
