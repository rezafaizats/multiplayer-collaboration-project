using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class Player_Input : MonoBehaviour
{
    [SerializeField] GameObject shortcutList;
    
    public Player_CameraWork cameraWork;
    public CharacterState state;

    public Player_RideHandler rideHandler;

    [SerializeField] LayerMask maskSnapAttack;

    [HideInInspector] float shotdelay = 1;
    public bool isAuthorized;

    [SerializeField] Collider targetedColider;

    //GameObject pauseMenu;



    [Header("Event")]
    [SerializeField] UnityEvent HideCursorTrigger;
    [SerializeField] UnityEvent ShowCursorTrigger;

    [SerializeField] UnityEvent QuitGameTrigger;
    
    [Header("Event Shortcut")]
    [SerializeField] UnityEvent Restart;

    void Start()
    {
        state.equipment.switchWeapon(state.anim.GetBool("IsRange"));
        SetAuthority(isAuthorized);
    }

    public void SetAuthority(bool authorize)
    {
        if(authorize)
        {
            isAuthorized = true;
            cameraWork.assignCamera();
            rideHandler.SetControl();
            cameraWork.StartControlCamera();
            //pauseMenu = GameObject.Find("Game Menu");
            //pauseMenu.SetActive(false);
            shortcutList = GameObject.Find("Shortcut List");
        }else
        {
            isAuthorized = false;
        }
    }

    public void hideCam()
    {
        if(isAuthorized)
        {
            cameraWork.hideAllCamera();
            cameraWork.setActiveCineBrain(false);
        }
    }
    public void showCam()
    {
        if(isAuthorized)
        {    
            cameraWork.showCamera();
            cameraWork.setActiveCineBrain(true);
        }
    }

    
    public void setControlable(bool controlable)
    {
        isControlable = controlable;
    }
    [SerializeField] bool isControlable = true;
    public bool getControlable()
    {
        return isAuthorized && isControlable;
    }
    void Update()
    {
        if(isAuthorized )
        {
            if(isControlable)
            {
                if(rideHandler.isInsideTheCar)
                {
                    ShortcutOnVehicle();
                }
                else
                {
                    Movement();
                    ShortcutOnfoot();
                }   
            }
            Shortcut();
        }
    }

    void Movement()
    {
        Transform cam = Camera.main.transform;
        Vector3 right = Vector3.right * Input.GetAxis("Horizontal");
        Vector3 forward = Vector3.forward * Input.GetAxis("Vertical");
        Vector3 direction = right+forward;

        if(cameraWork.isUseThirdPerson)
            state.characterMovement.Move(direction, cam.eulerAngles.y);
        else state.characterMovement.FPSMove(direction, cam.eulerAngles.y);
    }

    void ShortcutOnfoot()
    {
        ///Camera
        if(Input.GetKeyDown(KeyCode.F))
        {
            //Change Camera
            cameraWork.ChangeCamera();
        }
        //Jump
        if(Input.GetKeyDown(KeyCode.Space))
        {
            state.characterMovement.Jump();
        }
        //Switch Weapon
        if(Input.GetKeyUp(KeyCode.E))
        {
            //state.anim.ResetTrigger("Attack");
            bool isRange = state.anim.GetBool("IsRange");
            state.anim.SetBool("IsRange", !isRange);
            if(state.isUseNetwork)
            {
                state.equipment.switchWeaponNetwork(!isRange);
            }else
            {
                state.equipment.switchWeapon(!isRange);
            }
            
        }
        ///Attack
        if(Input.GetKeyDown(KeyCode.Q))
        {
            //Strike
            Collider[] cols =Physics.OverlapSphere(state.transform.position, 5, maskSnapAttack);
            if(cols.Length>0)
            {
                Collider colTarget = null;
                float lowest = 999;
                float tempDist = 999;
                foreach(Collider c in cols)
                {
                    if(c.gameObject!=state.gameObject)
                    {
                        tempDist = Vector3.Distance(state.transform.position, c.transform.position);
                        if(tempDist<lowest)
                        {
                            lowest = tempDist;
                            colTarget = c; 
                        }
                    }
                }
                if(colTarget!=null)
                {
                    targetedColider = colTarget;
                    state.characterMovement.FaceTo((colTarget.transform.position-state.transform.position), 0.01f);
                }
                else
                {
                    targetedColider = null;
                }
            }

            if(state.anim.GetBool("Moveable") && !state.anim.GetBool("IsRange"))
            {
                state.anim.SetTrigger("Attack");
            }
                
        }
        if(Input.GetKey(KeyCode.Q))
        {
            if(state.anim.GetBool("IsRange"))
            {
                if(shotdelay<=0)
                {
                    //state.anim.SetBool("Shoot", true);
                    state.anim.SetTrigger("Attack");
                    shotdelay = 0.25f;
                }
            }
        }
        shotdelay-=Time.deltaTime;

        ///GetInCar
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            //rideHandler.EnterCar();
        }

        ///For Test
        if(Input.GetKeyDown(KeyCode.K))
        {
            int random = Random.Range(1,4);
            state.anim.SetInteger("Random", random);
            state.anim.SetTrigger("Damage");
        }
        
    }
    void ShortcutOnVehicle()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            //rideHandler.ExitCar();
        }
    }
    
    bool isInConversiation = false;

    public void SetInConversiation(bool val)
    {
        isInConversiation = val;
    }
    void Shortcut()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            //Restart
            if(PhotonNetwork.IsMasterClient)
            {
                System.Action a =()=>
                {
                    //PhotonNetwork.LoadLevel(2);
                };
                Loader.fakeLoad(a);
            }

            if(Restart!=null)
            {
                Restart.Invoke();
                
                
            }else
            {
                Debug.Log("Tambahin Fungsi Restart di UnityEvent di script Player_Input");
            }
            
            
        }
        if(Input.GetKeyDown(KeyCode.V))
        {
            //MuteSFX
            Game_SoundConfig.Instance.muteSFX(!Game_SoundConfig.Instance.isMutedSFX());
           
        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            //Mute BGM
            Game_SoundConfig.Instance.muteBGM(!Game_SoundConfig.Instance.isMutedBGM());
            
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            //Show and Hide Shortcut
            if(shortcutList.activeSelf)
            {
                shortcutList.SetActive(false);
            }else
            {
                shortcutList.SetActive(true);
            }
        }
        if(Input.GetKeyDown(KeyCode.Tab)&&!isInConversiation)
        {
            state.characterMovement.StopMoveAnim();
            cameraWork.StopControlCamera();
            //pauseMenu.SetActive(true);
            //GameNetworkController.Instance.ShowPlayerList();
            GameNetworkController.Instance.playerListContainerParent.GetComponent<UIShowAnim>().open();
            GameNetworkController.Instance.ShowPlayerList();
            //pauseMenu.GetComponent<UIShowAnim>().open();
            isControlable =false;
            if(ShowCursorTrigger!=null)
            {
                ShowCursorTrigger.Invoke();
            }
        }
        if(Input.GetKeyUp(KeyCode.Tab)&&!isInConversiation)
        {
            state.characterMovement.StopMoveAnim();
            cameraWork.StartControlCamera();
            GameNetworkController.Instance.playerListContainerParent.GetComponent<UIShowAnim>().closeDontDestroy();
            //GameNetworkController.Instance.ShowPlayerList();
            //pauseMenu.GetComponent<UIShowAnim>().closeDontDestroy();
            isControlable = true;
            if(HideCursorTrigger!=null)
            {
                HideCursorTrigger.Invoke();
            }
        }
        
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            //Quit
            if(QuitGameTrigger!=null)
            {
                QuitGameTrigger.Invoke();
            }
            cameraWork.StopControlCamera();
        }

    }
}
