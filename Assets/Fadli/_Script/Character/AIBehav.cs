﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehav : MonoBehaviour
{
    public CharacterState state;
    public EnemyPatrol patrol;

    
    public GameObject currentTarget;

    [Header("Setting")]
    public bool isRange;
    public bool isUnarm = false;

    void Start()
    {
        if(isUnarm)
        {
            state.equipment.unarm();
        }
    }
    
    public void SetTarget(GameObject target)
    {
        currentTarget = target;
    }

    public bool isTargetExist()
    {
        if(currentTarget!=null)
        {
            if(currentTarget.GetComponent<CharacterState>().health>0)
                return true;
            else
            {
                currentTarget = null;
                return false;
            }
        }else return false;
    }

    public Vector3 getDirectionToCurrentTarget()
    {
        Vector3 pos = state.transform.position;
        Vector3 direction = currentTarget.transform.position - pos;

        return direction.normalized;
    }
    
    
}
