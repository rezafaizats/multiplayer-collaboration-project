﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] CharacterState state;

    public float turnSmoothTime =0.05f;
    float turnSmoothVelocity;

    [Header("Gravity")]
    [SerializeField] float gravity = 9.8f;
    [SerializeField] float currentSpeed_Y = 0;

    [Header("MovementSpeed")]
    [SerializeField] float maxSpeed = 5f;
    [SerializeField] float accel = 25f;
    //[SerializeField] float currentSpeed = 0;

    [Header("StairsCheck")]
    public LayerMask layerMask;
    bool isUpperFoot;
    bool isLowerFoot;

    public bool isUseGravity = true;


    void Start()
    {
        
    }

    public void SetSpeed(float accel, float maxSpeed)
    {
        this.accel = accel;
        this.maxSpeed = maxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        state.anim.SetFloat("SpeedY", currentSpeed_Y);
        state.transform.position+=Vector3.up*currentSpeed_Y*Time.deltaTime;
        if(isGrounded() && isUseGravity || !isUseGravity)
        {
            currentSpeed_Y = 0;
        }
        else
        {
            currentSpeed_Y-=gravity*Time.deltaTime;
        }
    }

    void StairsCheck()
    {

    }

    public bool isGrounded()
    {
        bool grounded = Physics.Raycast(state.transform.position + Vector3.up, Vector3.down, 1.05f, layerMask);
        state.anim.SetBool("Grounded", grounded);
        if(grounded)
            Debug.DrawRay(state.transform.position + Vector3.up, Vector3.down, Color.green);
        else  Debug.DrawRay(state.transform.position + Vector3.up, Vector3.down, Color.red);
        return grounded;
    }

    public void Jump()
    {
        if(isGrounded())
        {
            state.anim.Play("JumpStart");
            currentSpeed_Y = 5;
        }
    }

    public void StopMoveAnim()
    {
        state.anim.SetFloat("Speed", 0);
    }

    public void FaceTo(Vector3 direction, float turnSmooth = 0.2f)
    {
        float targetAngle=Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmooth);
        state.transform.rotation = Quaternion.Euler(0, angle ,0);
        state.anim.SetFloat("Speed", 0);
    }

    public void FPSMove(Vector3 direction, float camEulerY)
    {
        float targetAngle=Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + camEulerY;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
        state.transform.rotation = Quaternion.Euler(0, angle ,0);
        
        //Position
        if(!state.anim.GetBool("Moveable"))
        {
            return;
        }
        float currentSpeed = state.anim.GetFloat("Speed");
        if(direction.magnitude>0.1)
        {
            currentSpeed += accel*Time.deltaTime;
            if(currentSpeed>maxSpeed)
            {
                currentSpeed = maxSpeed;
            }
        }
        else
        {
            currentSpeed = 0;
            if(currentSpeed<0){currentSpeed=0;}
        }
        state.transform.position += transform.forward*currentSpeed*Time.deltaTime;
        //state.rigidb.velocity+= transform.forward*currentSpeed;

        //Animation
        state.anim.SetFloat("Speed", currentSpeed);
    }

    public void Move(Vector3 direction, float camEulerY)
    {        
        //Rotation
        if(direction.magnitude>0.01 && !state.isDeath)
        {
            float targetAngle=Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + camEulerY;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            state.transform.rotation = Quaternion.Euler(0, angle ,0);
        }
        

        //Position
        if(!state.anim.GetBool("Moveable"))
        {
            return;
        }
        float currentSpeed = state.anim.GetFloat("Speed");
        if(direction.magnitude>0.1)
        {
            currentSpeed += accel*Time.deltaTime;
            if(currentSpeed>maxSpeed)
            {
                currentSpeed = maxSpeed;
            }
        }
        else
        {
            currentSpeed = 0;
            if(currentSpeed<0){currentSpeed=0;}
        }
        state.transform.position += transform.forward*currentSpeed*Time.deltaTime;

        //Animation
        state.anim.SetFloat("Speed", currentSpeed);
        
    }
}
