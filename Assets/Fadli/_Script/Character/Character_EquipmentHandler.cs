﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Character_EquipmentHandler : MonoBehaviour
{
    [SerializeField] GameObject rangeWeapon;
    [SerializeField] GameObject meleeWeapon;

    [PunRPC]
    public void switchWeapon(bool toRangeWeapon)
    {
        if(toRangeWeapon)
        {
            rangeWeapon.SetActive(true);
            meleeWeapon.SetActive(false);
        }
        else
        {
            rangeWeapon.SetActive(false);
            meleeWeapon.SetActive(true);
        }
    }
    public void unarm()
    {
        rangeWeapon.SetActive(false);
        meleeWeapon.SetActive(false);
    }

    public void switchWeaponNetwork(bool toRangeWeapon)
    {
        switchWeapon(toRangeWeapon);
        GetComponent<PhotonView>().RPC("switchWeapon", RpcTarget.Others, toRangeWeapon);
    }
}
