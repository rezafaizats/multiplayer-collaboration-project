﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class OnDeath : MonoBehaviourPun
{

    public float delay = 3f;
    public GameObject deadNotif;
    public System.Action action;
    void Start()
    {

    }
    public void disconnect()
    {
        System.Action a =()=>
        {
            PhotonNetwork.Disconnect();
        };
        Loader.fadeOut(a);
    }

    void ShowNotification()
    {
        GameObject c = GameObject.Find("Canvas");
        GameObject g =Instantiate(deadNotif, c.transform);
        action = ()=>
        {
            disconnect();
        };
        g.GetComponent<UIShowAnim>().actionOnClick = action;
    }
    public void Action()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        StartCoroutine(waitToDisconnect());
        
    }
    IEnumerator waitToDisconnect()
    {
        ShowNotification();
        yield return new WaitForSeconds(delay);
        action.Invoke();
        
    }
}
