﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingAnimScript : MonoBehaviour
{
    public static AsyncOperation asyncOperator;
    [SerializeField] Text loadingText;
    [SerializeField] Slider loadingSlider;
    [SerializeField] Text loadingPercentage;

    [SerializeField] bool isCustomPersentage;

    void Start()
    {
        textAnim();
    }

    void textAnim()
    {
        System.Action c = ()=>
        {
            LeanTween.value(loadingText.gameObject, 0, 1, 0.5f).setOnUpdate((float val)=>
            {
            loadingText.text="Loading...";
            }).setOnComplete(textAnim);
        };
        System.Action b = ()=>
        {
            LeanTween.value(loadingText.gameObject, 0, 1, 0.5f).setOnUpdate((float val)=>
            {
            loadingText.text="Loading..";
            }).setOnComplete(c);
        };
        System.Action a = ()=>
        {
            LeanTween.value(loadingText.gameObject, 0, 1, 0.5f).setOnUpdate((float val)=>
            {
            loadingText.text="Loading.";
            }).setOnComplete(b);
        };
        LeanTween.value(loadingText.gameObject, 0, 1, 0.5f).setOnUpdate((float val)=>
        {
            loadingText.text="Loading";
        }).setOnComplete(a);
        
        
    }

    public static void setAsyncOperator(string SceneName, AsyncOperation op)
    {
        asyncOperator=op;
    }
    public void updatePercentageNumber(float val)
    {
        float newVal = val*100;
        loadingPercentage.text=newVal.ToString("00")+" %";
    }
    void Update()
    {
        if(isCustomPersentage)
        {
            return;
        }
        if(asyncOperator!=null)
        {
            loadingSlider.value = asyncOperator.progress;
        }
        else 
        {
            loadingSlider.value = Photon.Pun.PhotonNetwork.LevelLoadingProgress;
        }
        updatePercentageNumber(loadingSlider.value);
    }
}
