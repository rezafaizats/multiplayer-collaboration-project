﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTrigger : StateMachineBehaviour
{
    [System.Serializable]
    public class A_Boolean
    {
        public string name;
        public bool value;
    }

    [System.Serializable]
    public class A_Trigger
    {
        public string name;
    }
    
    [System.Serializable]
    public class A_Float
    {
        public string name;
        public float value;
    }

    [System.Serializable]
    public class A_Integer
    {
        public string name;
        public int value;
    }

    [System.Serializable]
    public class SetterAnim
    {
        public A_Float[] floats;
        public A_Integer[] integers;
        public A_Boolean[] booleans;
        public A_Trigger[] triggers;
    }

    public SetterAnim OnEnterTrigger;
    public SetterAnim OnExitTrigger;

    public SetterAnim OnUpdateTrigger;
    
    void StartTriggering(Animator animator, SetterAnim setterAnim)
    {
        if(setterAnim!=null)
        {
            if(setterAnim.floats!=null)
            {
                foreach(A_Float a in setterAnim.floats )
                {
                    animator.SetFloat(a.name, a.value);
                }
            }
            if(setterAnim.integers!=null)
            {
                foreach(A_Integer a in setterAnim.integers )
                {
                    animator.SetInteger(a.name, a.value);
                }
            }
            if(setterAnim.booleans!=null)
            {
                foreach(A_Boolean a in setterAnim.booleans)
                {
                    animator.SetBool(a.name, a.value);
                }
            }
            if(setterAnim.triggers!=null)
            {
                foreach(A_Trigger a in setterAnim.triggers)
                {
                    animator.SetTrigger(a.name);
                }
            }
        }
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!Photon.Pun.PhotonNetwork.IsMasterClient) return;
        StartTriggering(animator, OnEnterTrigger);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!Photon.Pun.PhotonNetwork.IsMasterClient) return;
        StartTriggering(animator, OnUpdateTrigger);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!Photon.Pun.PhotonNetwork.IsMasterClient) return;
        StartTriggering(animator, OnExitTrigger);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
