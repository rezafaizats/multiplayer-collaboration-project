﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Anim_AI_Patrol : StateMachineBehaviour
{
    AIBehav behav;
    [SerializeField] float waitTime = 5;
    [SerializeField] bool isNPC = false;
    float currentTime;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        if(!OnChangeHost.Instance.isMaster){return;}
        behav = animator.GetComponent<AIBehav>();
        if(!isNPC)
        {
            behav.state.fieldOfView.startFindTarget();
        }
        behav.state.characterMovement.SetSpeed(1.5f,3);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!OnChangeHost.Instance.isMaster){return;}
        if(behav==null)
        {
            behav = animator.GetComponent<AIBehav>();
        }
        if(behav.patrol.isArrived() && !animator.GetBool("Wait"))
        {
            behav.patrol.nextTarget();
            //if(behav==null) Debug.Log("behavNull");
            //if(behav.state==null) Debug.Log("stateNull");
            //if(behav.state.enemyNetworkController==null) Debug.Log("ENC null");
            //behav.state.enemyNetworkController.BroadcastNextPatrolTarget();
            animator.SetBool("Wait", true);
            currentTime = waitTime;
        }
        else if(currentTime >0)
        {
            currentTime-=Time.deltaTime;
        }
        else 
        {
            if(behav.patrol.GetTargetCount()>1)
            {
                animator.SetBool("Wait", false);
                behav.state.characterMovement.FPSMove(behav.patrol.getDirectionToCurrentTarget(), 0);
            }
            
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
          
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
