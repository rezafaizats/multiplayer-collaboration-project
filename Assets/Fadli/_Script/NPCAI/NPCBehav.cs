using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehav : AIBehav
{


    public LayerMask playerMask;
    public bool isTalking;
    public int random;
    public void lookAtTarget()
    {
        Vector3 direction = currentTarget.transform.position-state.transform.position;
        state.characterMovement.FaceTo(direction);
        
    }

    void Update()
    {
        if(isTalking)
        {
            if(!state.anim.GetBool("IsTalking"))
            {
                state.anim.SetInteger("Random", random);
                state.anim.SetBool("IsTalking", true);
                //state.anim.Play("Talk");
            }
            lookAtTarget();
        }else if(state.anim.GetBool("IsTalking"))
        {
            state.anim.SetBool("IsTalking", false);
        }
    }

    public void startConversiation()
    {
        Collider[] cols = Physics.OverlapSphere(state.transform.position, 5, playerMask);
        foreach(Collider c in cols)
        {
            if(c.tag=="Player")
            {
                SetTarget(c.gameObject);
                isTalking = true;
                return;
            }
        }
    }
    public void endConversiation()
    {
        isTalking = false;
        SetTarget(null);
    }
}
