﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCWalkAround : MonoBehaviour
{
    [SerializeField] AIBehav behav;
    [SerializeField] Transform [] targets;
    [SerializeField] float ArriveDistance = 1;
    Transform currentTarget;
    int currentIdx;

    void Awake()
    {
        currentIdx = 0;
        currentTarget = targets[currentIdx];
    }

    public Transform getCurrentTarget()
    {
        return currentTarget;
    }

    public Vector3 getDirectionToCurrentTarget()
    {
        Vector3 pos = behav.state.transform.position;
        Vector3 direction = currentTarget.position - pos;

        return direction.normalized;
    }

    public bool isArrived()
    {
        float distance = Vector3.Distance(currentTarget.position, behav.state.transform.position);
        if(distance>ArriveDistance)
        {
            return false;
        }else
        {
            return true;
        }
    }

    public void nextTarget()
    {
        if(currentIdx < targets.Length-1)
        {
            currentIdx++;
        }
        else
        {
            currentIdx = 0;
        }
        currentTarget = targets[currentIdx];
    }
}
