﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGSlideShow : MonoBehaviour
{
    [SerializeField] Sprite[] images;
    [SerializeField] Image backgroundImage;
    [SerializeField] Image transitionImage;
    [SerializeField] float changeDelayInSecond = 20;
    [SerializeField] float transitionTime = 5;
    [SerializeField] bool randomChange = false;
    int currentImage = 0;
    void Start()
    {
        if(randomChange)
        {
            changeBackgroundRandom();
        }
        StartCoroutine(ChangeDelay());    
    }
    IEnumerator transition()
    {
        float tempA = 0;
        float tempB = 0;
        Color c = Color.black;
        c.a = 0;
        while(tempA<transitionTime/2)
        {
            
            tempA+=Time.deltaTime;
            c.a = tempA/(transitionTime/2);
            transitionImage.color = c;
            //Debug.Log(tempA);
            yield return null;
        }
        if(randomChange)
        {
            changeBackgroundRandom();
        }
        else
        {
            nextBackground();
        }
        while(tempB<transitionTime/2)
        {
            tempB+=Time.deltaTime;
            c.a = 1-tempB/(transitionTime/2);
            transitionImage.color = c;
            //Debug.Log(1-tempB);
            yield return null;
        }
    }
    IEnumerator ChangeDelay()
    {
        while(true)
        {
            yield return new WaitForSeconds(changeDelayInSecond);
            yield return StartCoroutine(transition());
        }
    }

    void changeBackgroundRandom()
    {
        int random = Random.Range(0, images.Length-1);
        if(random == currentImage)
        {
            nextBackground();
        }else
        {
            changeBackground(random);
        }
    }

    void changeBackground(int index)
    {
        backgroundImage.sprite = images[index];
        currentImage = index;
    }

    void nextBackground()
    {
        if(currentImage+1<images.Length)
        {
            changeBackground(currentImage+1);
        }else
        {
            currentImage = 0;
            changeBackground(0);
        }
    }
}
