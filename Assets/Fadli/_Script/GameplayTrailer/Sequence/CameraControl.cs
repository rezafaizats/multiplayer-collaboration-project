﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class CameraControl : MonoBehaviour
{
    [System.Serializable]
    public class CameraSequence
    {
        public string name;
        public float timeToShoot;
        public GameObject camera;
        public UnityEvent startShoot;
    }

    [SerializeField]
    public CameraSequence[] sequences;
    int currentSequence;

    GameObject[] Vcameras;

    public bool playOnStart = true;

    public UnityEvent EndTrigger;

    void Awake()
    {
        Vcameras = GameObject.FindGameObjectsWithTag("VirtualCam");
    }

    void Start()
    {
        if(playOnStart)
            StartCoroutine(sequencePlay());
    }
    public void startAnim()
    {
        StartCoroutine(sequencePlay());
    }
    IEnumerator sequencePlay()
    {
        //activeCam(currentSequence);
        reset();
        sequences[currentSequence].camera.SetActive(true);
        if(sequences[currentSequence].startShoot!=null)
        {
            sequences[currentSequence].startShoot.Invoke();
        }
        yield return new WaitForSeconds(sequences[currentSequence].timeToShoot);
        currentSequence++;
        if(currentSequence<sequences.Length)
        {
          StartCoroutine(sequencePlay());  
        }
        else
        {
            EndTrigger.Invoke();
            yield break;
        }
        
    }
    void reset()
    {
        foreach(GameObject cam in Vcameras)
        {
            cam.SetActive(false);
        }
    }

    void activeCam(int id)
    {
        for(int i=0;i<Vcameras.Length;i++)
        {
            if(i==id)
            {
                Vcameras[i].SetActive(true);
            }
            else
            {
                Vcameras[i].SetActive(false);
            }
        }
    }
}
