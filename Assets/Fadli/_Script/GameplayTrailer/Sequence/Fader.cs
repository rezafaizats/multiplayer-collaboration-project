﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;

public class Fader : MonoBehaviour
{
    public enum FLASHCOLOR
    {
        black,
        white,
        grey,
        red
    }
    public float inTime=1;
    public float outTime=1;
    
    public FLASHCOLOR flashColor = FLASHCOLOR.black;
    public UnityEvent afterFadeOut;
    public UnityEvent afterFadeIn;

    Color color;
    public void startFade()
    {
        colorSelect();
        System.Action before =()=>
        {
            afterFadeOut.Invoke();
        };
        System.Action after =()=>
        {
            afterFadeIn.Invoke();
        };
        fakeLoad(before, after, inTime,outTime, color);
    }
    public static void fakeLoad(Action doAfterFadeOut, Action doAfterFadeIn, float inTime, float outTime, Color color)
    {
        GameObject g = new GameObject();
        g.SetActive(false);
        g.name = "fading screen";
        color.a = 0;
        g.AddComponent<Canvas>().gameObject.AddComponent<Image>().color = color;
        Canvas c = g.GetComponent<Canvas>();
        Image img = g.GetComponent<Image>();
        
        Action afterFadeInComplete=()=>
        {
            if(doAfterFadeIn!=null) doAfterFadeIn.Invoke();
        };
        Action fadein = ()=>
        {
            if(doAfterFadeOut!=null) doAfterFadeOut.Invoke();
            LeanTween.value(g, 1, 0, inTime).setOnUpdate((float val)=>
            {
            color.a = val;
            img.color = color;
            }).setOnComplete(afterFadeInComplete);
        };
        LeanTween.value(g, 0, 1, outTime).setOnUpdate((float val)=>
        {
            if(g.activeSelf)
            {
                color.a = val;
                img.color = color;
            }else
            {
                g.SetActive(true);
                c.renderMode = RenderMode.ScreenSpaceOverlay;
            }
        }).setOnComplete(fadein);
    }
    void colorSelect()
    {
        switch(flashColor)
        {
            case FLASHCOLOR.white:
                color = Color.white;
            break;
            case FLASHCOLOR.black:
                color = Color.black;
            break;
            case FLASHCOLOR.grey:
                color = Color.grey;
            break;
            case FLASHCOLOR.red:
                color = Color.red;
            break;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
    
}
