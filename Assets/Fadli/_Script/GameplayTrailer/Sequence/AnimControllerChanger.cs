﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerChanger : MonoBehaviour
{
    [System.Serializable]
    public struct AnimCtrlChanger
    {
        public string name;
        public Animator animator;
        public RuntimeAnimatorController newController;
    }

    public AnimCtrlChanger[] animCtrlChangers;

    public void changeController()
    {
        foreach(AnimCtrlChanger anim in animCtrlChangers)
        {
            anim.animator.runtimeAnimatorController = anim.newController;
        }
    }
}
