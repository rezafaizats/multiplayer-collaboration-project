﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class StartAnimator : MonoBehaviour
{   
    [System.Serializable]
    public struct animDesc
    {
        public string animName;
        public float duration;
        [Range(0,1)]
        public float transitionNormDur;
        public UnityEvent trigger;
    }
    [SerializeField] Animator animator;
    public bool playAnimatorFromStart = true;

    [SerializeField] animDesc[] animSeq;
    int currentIdxSequence;

    

    void Awake()
    {
        if(animator==null)
        {
            animator = GetComponent<Animator>();
        }
        UpdateAnimClipTimes();
        animator.enabled = false;
    }
    void Start()
    {
        if(playAnimatorFromStart)
        {
            animator.enabled =true;
        }
    }
    public void startAnimController()
    {
        animator.enabled = true;
    }

    public void playTriggerAnimationSequence()
    {
        StopAllCoroutines();
        StartCoroutine(animSequenceIE(currentIdxSequence));
    }
    IEnumerator animSequenceIE(int index)
    {
        if(animSeq[index].trigger!=null)
        {
            animSeq[index].trigger.Invoke();
        }
        animator.CrossFade(animSeq[index].animName, animSeq[index].transitionNormDur );
        yield return new WaitForSeconds(animSeq[index].duration);
        currentIdxSequence++;
        playTriggerAnimationSequence();
    }

    public void UpdateAnimClipTimes()
    {
        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        for(int i=0; i<animSeq.Length;i++)
        {
            foreach(AnimationClip clip in clips)
            {
                if(animSeq[i].animName == clip.name)
                {
                    animSeq[i].duration = clip.length;
                }
            }      
        }
    }
        

}
