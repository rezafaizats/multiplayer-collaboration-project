﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimProp : MonoBehaviour
{
    public enum leanAnimEase
    {
        linear,
        easeInOut,
        
    }
    [System.Serializable]
    public class animationTranslate
    {
        public GameObject target;
        public float duration;
        public Vector3 direction;
        public float to = 1;
        public Transform toLocation;
        public leanAnimEase leanType = leanAnimEase.linear;

        public UnityEvent trigger;
    }

    [System.Serializable]
    public class animationRotation
    {
        public GameObject target;
        public float duration;
        public Vector3 rotationUp = Vector3.up;

        public float degree = 90;
        public UnityEvent trigger;
    }
    
    public bool oneAnimPerSequence = true;
    public animationTranslate[] animTrans;
    public animationRotation[] animRots;


    LTDescr lean;

    void reset()
    {
        
    }
    public void translateObject(int id)
    {
        if(oneAnimPerSequence){
            if(lean!=null)
                LeanTween.cancel(lean.id);
        }
        animationTranslate anim =animTrans[id];
        System.Action onComplete=()=>{transOnComplete(id);};
        if(anim.toLocation!=null)
        {
            if(anim.leanType==leanAnimEase.linear)
                lean = LeanTween.moveLocal(anim.target, anim.toLocation.position, anim.duration).setOnComplete(onComplete);
            else if(anim.leanType==leanAnimEase.easeInOut)
            {
                lean = LeanTween.moveLocal(anim.target, anim.toLocation.position, anim.duration).setEaseInOutCubic().setOnComplete(onComplete);
            }
        }
        else
        {
            if(anim.leanType==leanAnimEase.linear)
                lean = LeanTween.moveLocal(anim.target, anim.direction*anim.to, anim.duration).setOnComplete(onComplete);
            else if(anim.leanType==leanAnimEase.easeInOut)
                lean = LeanTween.moveLocal(anim.target, anim.direction*anim.to, anim.duration).setEaseInOutCubic().setOnComplete(onComplete);
           
        }
    }
    public void rotateObject(int id)
    {
        if(oneAnimPerSequence)
        {
            if(lean!=null)
                LeanTween.cancel(lean.id);
        }
        animationRotation anim =animRots[id];
        System.Action onComplete=()=>{rotsOnComplete(id);};
        LeanTween.rotateAroundLocal(anim.target, anim.rotationUp, anim.degree, anim.duration).setOnComplete(onComplete);
    }
    void rotsOnComplete(int id)
    {
        if(animRots[id].trigger!=null)
        {
            animRots[id].trigger.Invoke();
        }
    }
    void transOnComplete(int id)
    {
        if(animTrans[id].trigger!=null)
        {
            animTrans[id].trigger.Invoke();
        }
    }


}
