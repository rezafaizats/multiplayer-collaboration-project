﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner Instance;
    
    public RouteList[] targets;

    public GameObject[] enemies;

    [SerializeField] GameObject[] spawnedEnemies;

    float delayPerinstance = 1.5f;

    void Awake()
    {
        if(Instance==null){Instance = this;}
        else{Destroy(this.gameObject);}
    }
    void Start()
    {
        spawnedEnemies = new GameObject[targets.Length];
        if(PhotonNetwork.IsMasterClient)
        {
            //Debug.Log("DoRepeatingEnemySpawn");
            //InvokeRepeating("DelaySpawn", 0.5f, 1f);
        
            Debug.Log("Coba Instance");
            StartCoroutine(delaySpawnAsync());

       }
       else
       {
           Debug.Log("FindEnemyHere");
           StartCoroutine(findEnemy());
       }
    }

    IEnumerator findEnemy()
    {
        while(true)
        {
            EnemyBehav[] behavs = GameObject.FindObjectsOfType<EnemyBehav>();
            OnChangeHost.Instance.setEnemyRange(behavs);
            yield return new WaitForSeconds (5);
        }
    }

    IEnumerator delaySpawnAsync()
    {
        bool isAll = true;
        while (isAll)
        {
            string a="";
            isAll = true;
            for(int i = 0; i<targets.Length;i++)
            {
                if(spawnedEnemies[i]!=null) {
                    a+=spawnedEnemies[i].name+" already spawn\n";
                }
                else
                {
                    int random = Random.Range(0, enemies.Length-1);
                    GameObject g = PhotonNetwork.InstantiateRoomObject(enemies[random].name, targets[i].spawn.position, targets[i].spawn.rotation);
                    g.GetComponent<EnemyNetworkController>().BroadcastSetEnemyIndex(i);
                    EnemyBehav b = g.GetComponent<CharacterState>().enemyBehav;
                    spawnedEnemies[i] = g;
                    OnChangeHost.Instance.addEnemy(b);
                    isAll = false;
                    a+="spawning "+g.name;
                }
            }
            Debug.Log(a);
            yield return new WaitForSeconds(delayPerinstance);
        }
    }
    void DelaySpawn()
    {
        
        string a = "";
        for(int i = 0; i<targets.Length;i++)
        {
            if(spawnedEnemies[i]!=null) {
                a+=spawnedEnemies[i].name+" already spawn\n";
            }
            else
            {
                int enemyIdx = Random.Range(0, enemies.Length-1);
                GameObject g = PhotonNetwork.Instantiate(enemies[i].name, targets[i].spawn.position, targets[i].spawn.rotation);
                g.GetComponent<EnemyNetworkController>().BroadcastSetEnemyIndex(i);
                //b.SetPatrolTarget(targets[i].route);
                spawnedEnemies[i] = g;
                if(g!=null)
                {
                    EnemyBehav b = g.GetComponent<CharacterState>().enemyBehav;
                    if(b!=null)
                        OnChangeHost.Instance.addEnemy(b);
                }
                a+="spawning "+g.name+"\n";
            }
            Debug.Log(a);
        }
    }

    public void UpdateLogic(EnemyBehav b)
    {
        b.SetPatrolTarget(targets[b.enemyIdx].route);
    }
}
