﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject> {
}
public class FieldOfView : MonoBehaviour
{


    [Header("Init")]
    public float hearRadius;
    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;
    public float defaultDelay;

    [Header("Mask and Tag")]
    public LayerMask targetMask;
    public LayerMask obstacleMask;
    [SerializeField] bool useTag;
    [SerializeField] string tagName;
    [SerializeField] float seenCoolDown;

    [SerializeField] GameObjectEvent OnFoundTarget;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    [HideInInspector]
    public Vector3 lastHeardTarget;
    [HideInInspector]
    public bool heard;
    IEnumerator cr;
    //void Awake() { cr = FindVisibleWithDelay(5); }
    
    public void startFindTarget(float delay)
    {
        if(cr!=null)
        {
            StopCoroutine(cr); 
        }
        cr = FindVisibleWithDelay(delay);
        StartCoroutine(cr);
    }
    public void startFindTarget()
    {
        if(cr!=null)
        {
            StopCoroutine(cr); 
        }
        cr = FindVisibleWithDelay(0.2f);
        StartCoroutine(cr);
    }

    public void stopFindTarget() {
        StopCoroutine(cr);
        visibleTargets.Clear();
    }


    IEnumerator FindVisibleWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }


    void FindVisibleTargets() {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        for (int i = 0; i < targetsInViewRadius.Length; i++) {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2) {
                float distToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dirToTarget, distToTarget, obstacleMask)) {
                    if (useTag)
                    {
                        if(target.CompareTag(tagName))visibleTargets.Add(target);
                        return;
                    }
                    else
                    {
                        visibleTargets.Add(target);
                    }
                }
            }
        }
    }


    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal) {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
    
}
