﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public class EnemyBehav : AIBehav
{
    
    public EnemyRangeCombat rangeCombat;
    public EnemyMeleeCombat meleeCombat;
    
    [Header("Setting")]
    public bool isAggresiveMode;

    public int enemyIdx;

    public void SetIdxRPC(int i)
    {
        enemyIdx = i;
    } 

    public void SetPatrolTarget(Transform[] targets)
    {
        patrol.SetTargets(targets);
    }

    

    void Start()
    {
        
        state.anim.SetBool("IsAggressive", isAggresiveMode);
        if(!isUnarm)
        {
            state.anim.SetBool("IsRange", isRange);
            state.equipment.switchWeapon(isRange);
        }else
        {
            state.equipment.unarm();
        }
    }
    
    
    void calculateDistanceWithTarget()
    {
        Vector3 pos = state.transform.position;
        Vector3 direction = currentTarget.transform.position - pos;
        
        state.anim.SetFloat("TargetDistance", direction.magnitude);
    }

    
    void TargetFinding()
    {
        if(state.isUseNetwork)
        {
            if(PhotonNetwork.IsMasterClient)
            {
                if(isAggresiveMode)
                {
                    if(state.fieldOfView.visibleTargets.Count>0)
                    {
                        state.GetComponent<EnemyNetworkController>().BroadcastSetTarget
                        (
                            state.fieldOfView.visibleTargets[0].gameObject
                        );
                    }
                }
            }
        }
        else
        {
            if(isAggresiveMode)
            {
                if(state.fieldOfView.visibleTargets.Count>0)
                {
                    SetTarget(state.fieldOfView.visibleTargets[0].gameObject);
                }
            }
        }
        
    }

    

    // Update is called once per frame
    void Update()
    {
        if(!PhotonNetwork.IsMasterClient){return;}

        state.anim.SetBool("IsRange", isRange);
        state.anim.SetBool("IsAggressive", isAggresiveMode);

        if(isTargetExist())
        {
            state.anim.SetBool("IsFoundTarget", true);
            calculateDistanceWithTarget();
        }
        else
        {
            state.anim.SetBool("IsFoundTarget", false);
            state.anim.SetFloat("TargetDistance", 999);
            TargetFinding();
        }

        if(!state.anim.GetBool("IsFoundTarget"))
        {

        }
    }
}
