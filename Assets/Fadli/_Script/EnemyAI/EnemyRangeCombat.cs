﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeCombat : MonoBehaviour
{
    [SerializeField] AIBehav behav;
    public float attackRangeRadius = 5;
    
    float rangeAttackDelay=0.25f;
    float reloadTime = 3;
    float currentDelay = 0;
    bool abletoShot = false;

    int ammo = 5;

    bool combatStarted = false;


    // Start is called before the first frame update

    public void Shoot()
    {
        if(currentDelay<=0)
        {
            abletoShot = true;
        }else
        {
            currentDelay-=Time.deltaTime;
        }
        if(abletoShot)
        {
            if(ammo>0)
            {
                //behav.state.anim.SetBool("Shoot", true);
                behav.state.anim.SetTrigger("Attack");
                ammo--;
                abletoShot = false;
                currentDelay = rangeAttackDelay;
            }
            else
            {
                ammo = 5;
                currentDelay = reloadTime;
                abletoShot = false;
            }
        }
        
    }
    void Start()
    {
        
    }

    void Combat()
    {
        if(behav.state.anim.GetFloat("TargetDistance")<=attackRangeRadius )
        {
            behav.state.characterMovement.FaceTo(behav.getDirectionToCurrentTarget());
            Shoot();
            //animator.SetBool("Shoot", true);
        }
        else
        {
            behav.state.characterMovement.FPSMove(behav.getDirectionToCurrentTarget(),0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(behav.state.anim.GetBool("IsRangeCombatStarted"))
        {
            combatStarted = true;
            if(behav.state.anim.GetBool("IsFoundTarget"))
            {
                Combat();
            }
            else
            {
                behav.state.anim.SetBool("IsRangeCombatStarted", false);
            }
        }else if(combatStarted)
        {
            combatStarted = false;
            behav.state.anim.SetFloat("Speed", 0);
        }
    }
}
