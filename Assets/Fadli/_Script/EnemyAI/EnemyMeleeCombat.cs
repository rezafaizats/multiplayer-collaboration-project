﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeCombat : MonoBehaviour
{
    [SerializeField] EnemyBehav behav;
    public float attackMeleeRadius = 1.25f;

    public float meleeAttackDelay=1;

    float currentDelay = 0;
    bool ableToSlash = false;
    bool combatStarted = false;

    public void Slash()
    {
        if(currentDelay<=0)
        {
            ableToSlash = true;
        }else
        {
            currentDelay-=Time.deltaTime;
        }
        if(ableToSlash)
        {
            behav.state.anim.SetTrigger("Attack");
            ableToSlash = false;
            currentDelay = meleeAttackDelay;
        }
        
    }
    void Start()
    {
        
    }

    void Combat()
    {
        if(behav.state.anim.GetFloat("TargetDistance")<=behav.meleeCombat.attackMeleeRadius )
        {
            behav.state.characterMovement.FaceTo(behav.getDirectionToCurrentTarget());
            Slash();
            //animator.SetBool("Shoot", true);
        }
        else
        {
            behav.state.characterMovement.FPSMove(behav.getDirectionToCurrentTarget(),0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(behav.state.anim.GetBool("IsMeleeCombatStarted"))
        {
            if(behav.state.anim.GetBool("IsFoundTarget"))
            {
                Combat();
            }
            else
            {
                behav.state.anim.SetBool("IsMeleeCombatStarted", false);
            }
        }else if(combatStarted)
        {
            combatStarted = false;
            behav.state.anim.SetFloat("Speed", 0);
        }
    }
}
