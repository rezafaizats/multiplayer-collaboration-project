﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] AIBehav behav;
    [SerializeField] Transform [] targets;
    [SerializeField] float ArriveDistance = 1;
    Transform currentTarget;
    int currentIdx;

    void Awake()
    {
        currentIdx = 0;
        if(targets.Length>0)
        {
            currentTarget = targets[currentIdx];
        }
        else
        {
            currentTarget = this.transform;
        }
    }
    public void SetTargets(Transform[] targets)
    {
        this.targets = targets;
        currentTarget = targets[0];
    }

    public int GetTargetCount()
    {
        if(targets!=null)
        {
            return targets.Length;
        }
        return 0;
    }
    public Transform getCurrentTarget()
    {
        return currentTarget;
    }

    public Vector3 getDirectionToCurrentTarget()
    {
        Vector3 pos = behav.state.transform.position;
        Vector3 direction = currentTarget.position - pos;

        return direction.normalized;
    }

    public bool isArrived()
    {
        float distance = Vector3.Distance(currentTarget.position, behav.state.transform.position);
        if(distance>ArriveDistance)
        {
            return false;
        }else
        {
            return true;
        }
    }

    public void nextTarget()
    {
        if(currentIdx < targets.Length-1)
        {
            currentIdx++;
        }
        else
        {
            currentIdx = 0;
        }
        currentTarget = targets[currentIdx];
        //photonView.RPC("SetCurrentTargetIdxRPC", RpcTarget.All, currentIdx);
    }

    //[PunRPC]
    //public void SetCurrentTargetIdxRPC(int i)
    //{
    //    currentTarget = targets[i];
    //}
    
}
