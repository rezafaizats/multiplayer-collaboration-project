﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FungsiDadakan : MonoBehaviour
{
    [Header("Game Console")]
    [SerializeField] KeyCode consoleKey = KeyCode.F3;
    [SerializeField] int countShowUp = 10;
    [SerializeField] GameObject consoleGO;
    int count;

    void Start()
    {
        DontDestroyOnLoad(this);
        reset();
    }
    void reset()
    {
        count = 0;
        consoleGO.SetActive(false);
    }
    void Update()
    {
        if(Input.GetKeyDown(consoleKey))
        {
            count++;
            if(count>=countShowUp)
            {
                consoleGO.SetActive(!consoleGO.activeSelf);
                reset();
            }
        }
    }




}
