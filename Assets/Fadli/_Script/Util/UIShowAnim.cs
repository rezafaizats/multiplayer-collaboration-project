﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShowAnim : MonoBehaviour
{
    [SerializeField] bool showOnEnable = true;

    public System.Action actionOnClick;

    public void onClickDoSomeThing()
    {
        actionOnClick.Invoke();
    }
    void Start()
    {
        //this.gameObject.SetActive(false);
        
    }
    void OnEnable()
    {
        //this.gameObject.transform.localScale= new Vector3(0.000001f,0.000001f,1);
        this.gameObject.transform.localScale = Vector3.zero;
        if(showOnEnable)
            open();
    }
    public void open(System.Action action)
    {
        LeanTween.cancel(this.gameObject);
        System.Action anim2=()=>
        {
            LeanTween.scaleY(this.gameObject, 1, 0.01f).setEaseInOutCubic().setOnComplete(action);
        };
        LeanTween.scaleX(this.gameObject, 1, 0.01f).setEaseInOutCubic().setOnComplete(anim2);
    }
    public void open()
    {
        LeanTween.cancel(this.gameObject);
        System.Action anim2=()=>
        {
            LeanTween.scaleY(this.gameObject, 1, 0.01f).setEaseInOutCubic();
        };
        LeanTween.scaleX(this.gameObject, 1, 0.01f).setEaseInOutCubic().setOnComplete(anim2);
    }

    public void close()
    {
        LeanTween.cancel(this.gameObject);
        System.Action destroy=()=>{Destroy(this.gameObject);};
        System.Action anim2=()=>
        {
            LeanTween.scaleX(this.gameObject, 0, 0.1f).setEaseInOutCubic().setOnComplete(destroy);
        };
        LeanTween.scaleY(this.gameObject, 0.01f, 0.1f).setEaseInOutCubic().setOnComplete(anim2);
    }
    public void closeDontDestroy()
    {
        LeanTween.cancel(this.gameObject);
        System.Action anim2=()=>
        {
            LeanTween.scaleX(this.gameObject, 0, 0.1f).setEaseInOutCubic();
        };
        LeanTween.scaleY(this.gameObject, 0.01f, 0.1f).setEaseInOutCubic().setOnComplete(anim2);
    }
    public void closeDontDestroy(System.Action action)
    {
        LeanTween.cancel(this.gameObject);
        System.Action anim2=()=>
        {
            LeanTween.scaleX(this.gameObject, 0, 0.1f).setEaseInOutCubic().setOnComplete(action);
        };
        LeanTween.scaleY(this.gameObject, 0.01f, 0.1f).setEaseInOutCubic().setOnComplete(anim2);
    }
    void OnDisable()
    {
        
    }
}
