﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VehicleSelector : MonoBehaviour
{
    [SerializeField] Player_Input input;
    [SerializeField] Player_RideHandler rideHandler;
    [SerializeField] LayerMask selectableMask;
    [SerializeField] float maxRayDistance =30;
    [SerializeField] bool isDebugging=false;

    [SerializeField] UnityEvent InteractEvent;

    [SerializeField] KeyCode InteractButton = KeyCode.Mouse0;

    //[SerializeField] bool rideOverTheNetwork = false;
    //[SerializeField] Photon.Pun.PhotonView photonPlayer;

    VehicleHopper currentHopper;

    void raycasting()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
        RaycastHit info;
        if(isDebugging)
            Debug.DrawRay(ray.origin, ray.direction, Color.red);
        if(Physics.Raycast(ray, out info, maxRayDistance, selectableMask))
        {
            if(GetComponentInParent<CharacterState>().isUseNetwork)
            {
                if(info.collider.gameObject.tag=="Vehicle")
                {
                    VehicleHopper vh = info.collider.gameObject.GetComponent<VehicleHopper>();
                    if(vh!=null)
                    {
                        //vh.rideHandlerRequester = rideHandler;
                        rideHandler.hopper = vh;
                        //rideHandler.vehicleCode = vh.vehicleScript;
                        vh.EnterTheVehicleNetwork(rideHandler, Photon.Pun.PhotonNetwork.LocalPlayer);
                        Debug.Log("trying to ride online "+vh.vehicleScript.gameObject.name);
                        InteractEvent.Invoke();
                        return;
                    }
                    Debug.Log("no vehicle Hopper in The Object ");
                    
                }
            }else
            {
                if(info.collider.gameObject.tag=="Vehicle")
                {
                    VehicleHopper vh = info.collider.gameObject.GetComponent<VehicleHopper>();
                    if(vh!=null)
                    {
                        rideHandler.hopper = vh;
                        rideHandler.vehicleCode = vh.vehicleScript;
                        Debug.Log("trying to ride offline "+vh.vehicleScript.gameObject.name);
                        rideHandler.EnterCar();
                        InteractEvent.Invoke();
                    }
                    
                }
            }
            
        }
    }

    void Update()
    {
        if(input.getControlable())
        {
            if(Input.GetKeyDown(InteractButton))
            {
                if(rideHandler.isInsideTheCar)
                {
                    if(GetComponentInParent<CharacterState>().isUseNetwork)
                    {
                        rideHandler.hopper.ExitTheVehiceNetwork();
                    }else
                    {
                        rideHandler.ExitCar();
                    }
                }
                else
                {
                    raycasting();
                }
            }
        }
        
    }
}
