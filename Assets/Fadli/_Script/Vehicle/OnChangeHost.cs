﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class OnChangeHost : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public static OnChangeHost Instance;
    public List<GameObject> masterOwnedObject;
    public List<ComputerControllerVehicle> masterOwnedCVeh;
    public List<EnemyBehav> masterOwnedEnemy;

    public bool isMaster;

    void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
            masterOwnedCVeh = new List<ComputerControllerVehicle>();
            masterOwnedEnemy = new List<EnemyBehav>();
            masterOwnedObject = new List<GameObject>();
            
        }
        else Destroy(this.gameObject);
    }
    void Start()
    {
        isMaster = PhotonNetwork.IsMasterClient;
    }
    public void setEnemyRange(EnemyBehav[] behavs)
    {
        masterOwnedEnemy.Clear();
        masterOwnedEnemy.AddRange(behavs);
    }
    public void addEnemy(EnemyBehav bhav)
    {
        masterOwnedEnemy.Add(bhav);
    }
    public void setVehicleRange(ComputerControllerVehicle[] cveh)
    {
        masterOwnedCVeh.Clear();
        masterOwnedCVeh.AddRange(cveh);
    }
    public void addVehicle(ComputerControllerVehicle cveh)
    {
        masterOwnedCVeh.Add(cveh);
    }
    public void addGameObject(GameObject obj)
    {
        masterOwnedObject.Add(obj);
    }
    public void addGameObjects( GameObject[] objects)
    {
        masterOwnedObject.AddRange(objects);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

    }

    public void updateLogic()
    {
        foreach(ComputerControllerVehicle c in masterOwnedCVeh)
        {
            SpawnComputerVehicle.Instance.UpdateLogic(c);
        }
        foreach(EnemyBehav b in masterOwnedEnemy)
        {
            EnemySpawner.Instance.UpdateLogic(b);                                   
        }
        
        isMaster = true;
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);

        if(PhotonNetwork.LocalPlayer == newMasterClient){
            updateLogic();
        }
    }
}
