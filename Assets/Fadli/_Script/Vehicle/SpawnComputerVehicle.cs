﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

public class SpawnComputerVehicle : MonoBehaviourPun
{
    public static SpawnComputerVehicle Instance;
    [SerializeField] RouteList[] movingVehiclesData;
    [Space]
    [SerializeField] GameObject movingVehiclePrefab;
    
    GameObject[] spawnedVehicle;

    float delayPerinstance = 1f;
    void Awake()
    {
        if(Instance==null)Instance =this;
        else Destroy(this);
    }
    void Start()
    {
        spawnedVehicle = new GameObject[movingVehiclesData.Length];
        if(PhotonNetwork.IsMasterClient)
        {
            /*
            foreach(RouteList p in movingVehiclesData)
            {
                GameObject g = PhotonNetwork.InstantiateRoomObject(movingVehiclePrefab.name, p.spawn.position, p.spawn.rotation);
                g.GetComponent<ComputerControllerVehicle>().route = p.route;
                g.GetComponent<ComputerControllerVehicle>().isRunning = true;
                OnChangeHost.Instance.addGameObject(g);
            }
            */
            StartCoroutine(DelaySpawn());
        }
        else
        {
            StartCoroutine(findVehicle());
        }
        
    }
    IEnumerator findVehicle()
    {
        while(true)
        {
            ComputerControllerVehicle[] c = GameObject.FindObjectsOfType<ComputerControllerVehicle>();
            OnChangeHost.Instance.setVehicleRange(c);
            yield return new WaitForSeconds (8);
        }
    }

    IEnumerator DelaySpawn()
    {
        bool isAll = true;
        while (isAll)
        {
            string a="";
            isAll = true;
            for(int i = 0; i<movingVehiclesData.Length;i++)
            {
                if(spawnedVehicle[i]!=null) {
                    a+=spawnedVehicle[i].name+" already spawn\n";
                }
                else
                {
                    GameObject g = PhotonNetwork.InstantiateRoomObject(movingVehiclePrefab.name, movingVehiclesData[i].spawn.position, movingVehiclesData[i].spawn.rotation);
                    ComputerControllerVehicle cVeh = g.GetComponent<ComputerControllerVehicle>();
                    cVeh.broadcastIdx(i);
                    cVeh.route = movingVehiclesData[i].route;
                    cVeh.isRunning = true;
                    spawnedVehicle[i] = g;
                    OnChangeHost.Instance.addVehicle(cVeh);
                    isAll = false;
                    a+="spawning "+g.name;
                }
            }
            Debug.Log(a);
            yield return new WaitForSeconds(delayPerinstance);
        }
        
    }
    public void UpdateLogic(ComputerControllerVehicle c)
    {
        c.route = movingVehiclesData[c.vehIdx].route;
        c.isRunning = true;
    }

}
