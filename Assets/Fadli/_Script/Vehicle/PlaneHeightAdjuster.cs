﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlaneBehaviour))]
public class PlaneHeightAdjuster : MonoBehaviour
{
    [SerializeField] LayerMask layer;
    [SerializeField] float upSpeed = 2f;
    [SerializeField] float distance = 10;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Physics.Raycast(this.transform.position+transform.forward, transform.forward, distance, layer ))
        {
            transform.position+=transform.up*upSpeed*Time.deltaTime;
        }
        else
        {
            Debug.DrawRay(this.transform.position+transform.forward, transform.forward* distance);
        }
    }
}
