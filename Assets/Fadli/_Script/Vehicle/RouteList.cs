﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RouteList : MonoBehaviour
{
    public Transform spawn;
    public Transform[] route;
    
}
