﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(MSVehicleControllerFree))]
[System.Serializable]
public class ComputerControllerVehicle : MonoBehaviour
{
    [System.Serializable]
    public class RayData
    {
        public RayData(string nameRay, float maxDist = 1)
        {
            this.nameRay = nameRay;
            this.maxDist = maxDist;
        }
        public string nameRay;
        public Transform initTransform;
        public float maxDist;
        public Vector3 getDir()
        {
            return initTransform.forward;
        }
        public Vector3 getPos()
        {
            return initTransform.position;
        }
        public float getDist()
        {
            return maxDist;
        }

    }
    
    [System.Serializable]
    public class RayCheck
    {
        public RayData frontMid;
        public RayData frontLeft;
        public RayData frontRight;

        public RayData frontLeft1;
        public RayData frontRight1;

        public RayData centerLeft;
        public RayData centerRight;
        public RayData backMid;
        public RayData backLeft;
        public RayData backRight;

    }
    MSVehicleControllerFree vehicleControllerFree;
    public RayCheck rays;

    public Transform[] route;

    public LayerMask obstacleMask;

    public int routeIdx;
    public float arrivedDistance = 2f;
    public float nearDistance = 10f;
    public float maxSpeed = 3f;
    [Header("Flags")]
    public bool isRunning;
    [Space]
    public bool isShowRay;
    [Space]

    [SerializeField] private bool isFrontMid;
    [SerializeField] private bool isFrontLeft;
    [SerializeField] private bool isFrontRight;

    [SerializeField] private bool isCenterLeft;
    [SerializeField] private bool isCenterRight;
    [SerializeField] private bool isBackMid;
    [SerializeField] private bool isBackLeft;
    [SerializeField] private bool isBackRight;

    float vFwd;
    //public bool test;
    //public bool test2;

    public int vehIdx;

    [PunRPC]
    public void updateRouteIdxRPC(int i)
    {
        routeIdx = i;
    }
    [PunRPC]
    public void UpdateRouteRPC(int route)
    {
        routeIdx = route;
    }

    [PunRPC]
    public void SetIdxRPC(int i)
    {
        vehIdx = i;
    } 

    public void broadcastIdx(int i)
    {
        GetComponent<PhotonView>().RPC("SetIdxRPC", RpcTarget.AllBuffered, i);
    }

    public void broadcastRoute(int routeIdx)
    {
        GetComponent<PhotonView>().RPC("UpdateRouteRPC", RpcTarget.AllBuffered, routeIdx);
    }

    void Start()
    {
        vehicleControllerFree = GetComponent<MSVehicleControllerFree>();
    }
    bool checkObstacle()
    {
        isFrontMid = Physics.Raycast(rays.frontMid.getPos(), rays.frontMid.getDir(), rays.frontMid.getDist(), obstacleMask);

        isFrontLeft =   Physics.Raycast(rays.frontLeft.getPos(), rays.frontLeft.getDir(), rays.frontLeft.getDist(), obstacleMask) || 
                        Physics.Raycast(rays.frontLeft1.getPos(), rays.frontLeft1.getDir(), rays.frontLeft1.getDist(), obstacleMask);
        isFrontRight =  Physics.Raycast(rays.frontRight.getPos(), rays.frontRight.getDir(), rays.frontRight.getDist(), obstacleMask) ||
                        Physics.Raycast(rays.frontRight1.getPos(), rays.frontRight1.getDir(), rays.frontRight1.getDist(), obstacleMask);

        isCenterLeft = Physics.Raycast(rays.centerLeft.getPos(), rays.centerLeft.getDir(), rays.centerLeft.getDist(), obstacleMask);
        isCenterRight = Physics.Raycast(rays.centerRight.getPos(), rays.centerRight.getDir(), rays.centerRight.getDist(), obstacleMask);
        isBackMid= Physics.Raycast(rays.backMid.getPos(), rays.backMid.getDir(), rays.backMid.getDist(), obstacleMask);
        isBackLeft = Physics.Raycast(rays.backLeft.getPos(), rays.backLeft.getDir(), rays.backLeft.getDist(), obstacleMask);
        isBackRight = Physics.Raycast(rays.backRight.getPos(), rays.backRight.getDir(), rays.backRight.getDist(), obstacleMask);

        Debug.DrawRay(rays.frontMid.getPos(), rays.frontMid.getDir() * rays.frontMid.getDist());
        Debug.DrawRay(rays.frontLeft.getPos(), rays.frontLeft.getDir()* rays.frontLeft.getDist());
        Debug.DrawRay(rays.frontLeft1.getPos(), rays.frontLeft1.getDir()* rays.frontLeft1.getDist());
        Debug.DrawRay(rays.frontRight.getPos(), rays.frontRight.getDir() *rays.frontRight.getDist());
        Debug.DrawRay(rays.frontRight1.getPos(), rays.frontRight1.getDir() *rays.frontRight1.getDist());
        Debug.DrawRay(rays.centerLeft.getPos(), rays.centerLeft.getDir()* rays.centerLeft.getDist());
        Debug.DrawRay(rays.centerRight.getPos(), rays.centerRight.getDir() *rays.centerRight.getDist());
        //Debug.DrawRay(rays.backMid.getPos(), rays.backMid.getDir()* rays.backMid.getDist());
        //Debug.DrawRay(rays.backLeft.getPos(), rays.backLeft.getDir() * rays.backLeft.getDist());
        //Debug.DrawRay(rays.backRight.getPos(), rays.backRight.getDir() * rays.backRight.getDist());

        return isFrontMid || isFrontLeft ||isFrontRight || isCenterLeft || isCenterRight /*||isBackMid|| isBackLeft|| isBackRight*/;

    }

    //returns -1 when to the left, 1 to the right, and 0 for forward/backward
    public float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);
        //Debug.Log(dir);
        //return dir;
        ///*
        if (dir > 1) {
            return 1.0f;
        } else if (dir < -1) {
            return -1.0f;
        } else {
            return 0.0f;
        }
        //*/
    }
    public float AngleDirRaw(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);
        return dir;
    }

    int getNextIdx()
    {
        if(routeIdx+1 < route.Length)
        {
            return routeIdx+1;
        }
        else
        {
            return 0;
        }
    }

    [SerializeField] bool isBackward = false;
    IEnumerator mundur()
    {
        Debug.Log("yuk kita mundur");
        isBackward = true;
        yield return new WaitForSeconds(1);
        float angle = AngleDirRaw (transform.forward, direction, transform.up);;
        while( angle > 2 || angle < -2)
        {
            angle = AngleDir (transform.forward, direction, transform.up);
            yield return null;
        }
        isBackward = false;
    }
    void obstacleAvoid()
    {
        int invert = 1;
        if(vFwd < 0.3 || isBackward)
        {
            if(!isBackward) 
            {
                StartCoroutine(mundur());
            }
            vehicleControllerFree.verticalInput = -1;
            invert =-1;
        }
        if(isFrontMid&&isFrontLeft)
        {
            vehicleControllerFree.horizontalInput = 1*invert;
        } 
        else if(isFrontMid&&isFrontRight)
        {
            vehicleControllerFree.horizontalInput = -1*invert;
        }
        else if(isFrontLeft)
        {
            vehicleControllerFree.horizontalInput = 1*invert;
        }
        else if(isFrontRight)
        {
            vehicleControllerFree.horizontalInput = -1*invert;
        }
        else if(isCenterLeft)
        {
            vehicleControllerFree.horizontalInput = 1*invert;
        }
        else if(isCenterRight)
        {
            vehicleControllerFree.horizontalInput = -1*invert;
        }
        
    }
    Vector3 direction;
    Vector3 nextDirection;
    void toDestination()
    {
        float dir = AngleDir(transform.forward, direction, transform.up);
        float nextDir = AngleDir(route[routeIdx].position, nextDirection, transform.up);

        if(direction.magnitude>arrivedDistance)
        {
            if(direction.magnitude<nearDistance)
            {
                if(nextDir>0||nextDir<0)
                {
                    if(vFwd > 5)
                    {
                        vehicleControllerFree.verticalInput = -1f;
                    }
                    else if(vFwd <2)
                    {
                        vehicleControllerFree.verticalInput = 1f;
                    }
                }
                else
                {
                    if(vFwd<maxSpeed)
                    {
                        vehicleControllerFree.verticalInput = 1f;
                    }
                    
                }
                
            }
            else
            {
                if(vFwd<maxSpeed)
                {
                    vehicleControllerFree.verticalInput = 1f;
                }
            }
            
            vehicleControllerFree.horizontalInput = dir;
        }
        else
        {
            if(routeIdx+1<route.Length)
            {
                routeIdx++;
                broadcastRoute(routeIdx);
            }else
            {
                routeIdx=0;
                broadcastRoute(routeIdx);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isRunning && OnChangeHost.Instance.isMaster)
        {
            direction = 
                new Vector3 (route[routeIdx].position.x, 0, route[routeIdx].position.z)-
                new Vector3 (this.transform.position.x, 0, this.transform.position.z);
            nextDirection = 
                new Vector3 (route[getNextIdx()].position.x, 0, route[getNextIdx()].position.z) -
                new Vector3 (route[routeIdx].position.x, 0, route[routeIdx].position.z);
            
            vFwd = new Vector3(vehicleControllerFree.ms_Rigidbody.velocity.x, 0, vehicleControllerFree.ms_Rigidbody.velocity.z).magnitude;
            
            bool obstaclei_isso = checkObstacle();
            if(obstaclei_isso || isBackward)
            {
                obstacleAvoid();
            }
            else
            {
                toDestination();
            }
            vehicleControllerFree.isInsideTheCar = true;
        }
    }

}
