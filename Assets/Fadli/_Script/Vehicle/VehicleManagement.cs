﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleManagement : MonoBehaviour
{
    // Start is called before the first frame update
    public static List<GameObject> vehicles;
    void Awake()
    {
        init();
    }
    void Start()
    {

    }

    void init()
    {
        vehicles = new List<GameObject>();
    }

    public static void findAllVehicles()
    {
        GameObject[] vehs = GameObject.FindGameObjectsWithTag("Vehicle");
        foreach (GameObject g in vehs)
        {
            vehicles.Add(g);
        }
    }

    public static void addVehicle(GameObject newVehicle)
    {
        if(!vehicles.Contains(newVehicle))
        {
            vehicles.Add(newVehicle);
        }
    }
    public static void removeVehicle(GameObject target)
    {
        foreach(GameObject veh in vehicles)
        {
            if(veh==target)
            {
                vehicles.Remove(target);
                return;
            }
        }
    }
    public static List<GameObject> getAllVehicles()
    {
        return vehicles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
