﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

public class VehicleHopper : MonoBehaviourPun, IPunOwnershipCallbacks
{
    // Start is called before the first frame update
    public MSVehicleControllerFree vehicleScript;
    
    [SerializeField] UnityEvent HoppingEvent;


    public Player_RideHandler rideHandlerRequester;

    [SerializeField] Player currentOwner;

    [SerializeField] string ownerName;
    public bool isUsedInNetwork;

    void Awake()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }
    void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    //[SerializeField] PhotonView pv;
    public void EnterTheVehicle()
    {
        if(isUsedInNetwork) 
        {
            Debug.LogError("Only Call EnterTheVehicle OfflineMode");
            return;
        }
        vehicleScript.EnterInVehicle();
        if(HoppingEvent!=null)
        {
            HoppingEvent.Invoke();
        }
    }

    public void ExitTheCar()
    {
        if(isUsedInNetwork) 
        {
            Debug.LogError("Only Call ExitTheVehicle OfflineMode");
            return;
        }
        vehicleScript.ExitTheVehicle();
    }

    public void ExitTheVehiceNetwork()
    {
        if(!isUsedInNetwork) 
        {
            Debug.LogError("Only Call ExitTheVehicleNetwork OnlineMode");
            return;
        }
        base.photonView.RPC("SetCurrentOwnerToNull", RpcTarget.All);
        rideHandlerRequester.ExitCarNetwork();
        rideHandlerRequester=null;
    }

    public void EnterTheVehicleNetwork(Player_RideHandler rideHandlerRequester, Player requester)
    {
        if(!isUsedInNetwork) 
        {
            Debug.LogError("Only Call EnterTheVehicleNetwork OnlineMode");
            return;
        }
        if(currentOwner==null && requester==base.photonView.Owner)
        {
            
            Debug.Log("currentOwner null, but the vehicle is owned by requester already");
            base.photonView.RPC("SetVehicleUserNetwork", RpcTarget.All, photonView.Owner);
            this.rideHandlerRequester=rideHandlerRequester;
            this.rideHandlerRequester.EnterCarNetwork();
        }
        else if(currentOwner==null)
        {
            this.rideHandlerRequester = rideHandlerRequester;
            base.photonView.RequestOwnership();
            Debug.Log("currentOwner null, make request to ride the vehicle");
        }
        else
        {
            this.rideHandlerRequester = null;
        }
        
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        if(targetView!= base.photonView) return;


        if(currentOwner==null)
        {
            targetView.TransferOwnership(requestingPlayer);
            Debug.Log("nbr "+requestingPlayer.ActorNumber+" request approved, transfering ownership");
            return;
        }else
        {
            Debug.Log("nbr "+requestingPlayer.ActorNumber+" request not meet the condition");
        }
    }
    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        if(targetView!= base.photonView) return;        
        if(rideHandlerRequester!=null)
        {
            targetView.RPC("SetVehicleUserNetwork", RpcTarget.All, targetView.Owner);
            rideHandlerRequester.EnterCarNetwork();
        }
        Debug.Log("ownership transfered to "+targetView.Owner.NickName+" from"+previousOwner.NickName);
    }
    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
    {
        Debug.Log(senderOfFailedRequest.NickName+"failed to get vehicle ownership");
        rideHandlerRequester = null;
    }

    [PunRPC]
    public void SetVehicleUserNetwork(Player Target)
    {
        currentOwner=Target;
        ownerName = currentOwner.ActorNumber.ToString();
    }

    [PunRPC]
    public void SetCurrentOwnerToNull()
    {
        currentOwner = null;
        ownerName = "Master I guess lol, i mean null, but not really, it's kinda null";
    }

}
