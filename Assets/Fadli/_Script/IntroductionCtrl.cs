﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RenderHeads.Media.AVProVideo;

public class IntroductionCtrl : MonoBehaviour
{
    
	public MediaPlayer _mediaPlayer;
    [SerializeField] UnityEvent readyToPlay;
    [SerializeField] UnityEvent started;
    [SerializeField] UnityEvent firstFrameReady;
    [SerializeField] UnityEvent finishPlaying;

    private void Start()
	{
		_mediaPlayer.Events.AddListener(OnMediaPlayerEvent);
	}

	private void OnDestroy()
	{
		_mediaPlayer.Events.RemoveListener(OnMediaPlayerEvent);
	}
	void Update()
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			finishPlaying.Invoke();
		}
	}
    public void OnMediaPlayerEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
		{
			switch (et)
			{
				case MediaPlayerEvent.EventType.ReadyToPlay:
                    if(readyToPlay!=null)readyToPlay.Invoke();
					break;
				case MediaPlayerEvent.EventType.Started:
                    if(started!=null)started.Invoke();
					break;
				case MediaPlayerEvent.EventType.FirstFrameReady:
                    if(firstFrameReady!=null)firstFrameReady.Invoke();
					break;
				case MediaPlayerEvent.EventType.FinishedPlaying:
                    if(finishPlaying!=null)finishPlaying.Invoke();
					break;
			}
		}
}
