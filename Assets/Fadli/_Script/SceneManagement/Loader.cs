﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public enum Scenes{
        Start,
        Loading,
        Introduction,
        Lobbby,
        GameScene
    }
public static class Loader
{

    private static Action onLoaderCallback;

    public static AsyncOperation async;

    public static void LoadSceneCustom(Action customLoadScene)
    {
        
        GameObject g = new GameObject();
        Canvas c = g.AddComponent<Canvas>();
        c.sortingOrder = 999;
        Color color = Color.black;
        color.a = 0;
        g.AddComponent<Image>().color = color;
        Image img = g.GetComponent<Image>();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        c.worldCamera = Camera.main;
        Action load =()=>
        {
            if(customLoadScene!=null) customLoadScene.Invoke();
        }; 
        LeanTween.value(g, 0, 1, 1).setOnUpdate((float val)=>
        {
            Color col = Color.black;
            col.a = val;
            img.color = col;
        }).setOnComplete(load);
    }

    public static void Load(Scenes scene, Action DoBeforeLoadScene)
    {
        Action load =()=>
        {
            if(DoBeforeLoadScene!=null) DoBeforeLoadScene.Invoke();
            SceneManager.LoadScene(scene.ToString());
        }; 
        fading(0,1,1, load);
    }

    public static GameObject fading(float from, float to, float time, Action act)
    {
        GameObject g = new GameObject();
        g.name = "fading screen";
        Canvas c = g.AddComponent<Canvas>();
        c.sortingOrder = 99;
        Color color = Color.black;
        color.a = 0;
        g.AddComponent<Image>().color = color;
        Image img = g.GetComponent<Image>();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        LeanTween.value(g, from, to, time).setOnUpdate((float val)=>
        {
            Color col = Color.black;
            col.a = val;
            img.color = col;
        }).setOnComplete(act);
        return g;
    }

    public static void fakeLoad(Action DoBeforeLoadScene)
    {
        GameObject g = new GameObject();
        Canvas c = g.AddComponent<Canvas>();
        c.sortingOrder = 999;
        Color color = Color.black;
        color.a = 0;
        g.AddComponent<Image>().color = color;
        Image img = g.GetComponent<Image>();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        //c.worldCamera = Camera.main;
         
        Action fadein = ()=>
        {
            if(DoBeforeLoadScene!=null) DoBeforeLoadScene.Invoke();
            LeanTween.value(g, 1, 0, 1).setOnUpdate((float val)=>
            {
            Color col = Color.black;
            col.a = val;
            img.color = col;
            });
        };
        LeanTween.value(g, 0, 1, 1).setOnUpdate((float val)=>
        {
            Color col = Color.black;
            col.a = val;
            img.color = col;
        }).setOnComplete(fadein);
    }
    public static void fadeIn(Action onComplete)
    {
        GameObject g = fading(1,0,1, onComplete);
        
        GameObject.Destroy(g, 5);
    }
    
    public static void fadeOut(Action onComplete)
    {
        GameObject g =fading(0,1,1, onComplete);
    }
    public static void fadeOutHalf(Action onComplete)
    {
        GameObject g= fading(0, 0.5f, 1, onComplete);
        GameObject.Destroy(g, 4);
    }


    public static void LoadWithLoading(Scenes scene)
    {
       
        onLoaderCallback =()=>{
            async = SceneManager.LoadSceneAsync(scene.ToString());
        };
        Action load = ()=>{
            
            SceneManager.LoadSceneAsync(Scenes.Loading.ToString(), LoadSceneMode.Additive);
        };
        //fadeOut(load);
    }
    
    public static void LoadWithLoadingHalfFade(Scenes scene)
    {
        onLoaderCallback =()=>{
            async = SceneManager.LoadSceneAsync(scene.ToString());
        };
        SceneManager.LoadSceneAsync(Scenes.Loading.ToString(), LoadSceneMode.Additive);
        
    }
    public static void OnLoaderCallback()
    {
        if(onLoaderCallback!=null)
        {
            onLoaderCallback();
            onLoaderCallback = null;
        }
    }

}
