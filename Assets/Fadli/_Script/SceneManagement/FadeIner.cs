﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeIner : MonoBehaviour
{
    public UnityEvent OnFadeInEnd;
    void Start()
    {
        System.Action a =()=>
        {
            if(OnFadeInEnd!=null)
            {
                OnFadeInEnd.Invoke();
            }
        };
        Loader.fadeIn(a);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
