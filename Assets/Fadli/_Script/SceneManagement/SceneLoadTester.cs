﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class SceneLoadTester : MonoBehaviour
{
    [SerializeField] string sceneToBeLoaded;
    [SerializeField] float waitToLoadScene = 3;
    LoadingAnimScript loadScript;
    [SerializeField] GameObject LoadingScreenPrefab;
    [SerializeField] bool executeInStart = true;
    
    void Start()
    {
        if(executeInStart)
            StartCoroutine(loadscene());
    }
    IEnumerator loadscene()
    {
        yield return new WaitForSeconds(waitToLoadScene);
        System.Action a =()=>
        {
            SceneManager.LoadSceneAsync("LoadingScene");
            LoadingAnimScript.setAsyncOperator(sceneToBeLoaded, SceneManager.LoadSceneAsync(sceneToBeLoaded));
        };
        Loader.fadeOut(a);
    }

    public void PhotonLoadSceneAsync()
    {
        System.Action a =()=>
        {
            loadScript = Instantiate(LoadingScreenPrefab, Vector3.zero, Quaternion.identity).GetComponent<LoadingAnimScript>();
            StartCoroutine(PhotonLoad());
        };
        Loader.fadeOut(a);
    }
    public void loadSceneNoLoadingScreen()
    {
        System.Action a =()=>
        {
            SceneManager.LoadScene(sceneToBeLoaded);
        };
        Loader.fadeOut(a);
    }
    IEnumerator PhotonLoad()
    {
        PhotonNetwork.LoadLevel(sceneToBeLoaded);

        while (PhotonNetwork.LevelLoadingProgress < 1)
        {
            //loadAmountText.text = "Loading: %" + (int)(PhotonNetwork.LevelLoadingProgress * 100);
            //Debug.Log("Loading: %" + (int)(PhotonNetwork.LevelLoadingProgress * 100));
            loadScript.updatePercentageNumber(PhotonNetwork.LevelLoadingProgress);
            //loadAmount = async.progress;
            //progressBar.fillAmount = PhotonNetwork.LevelLoadingProgress;
            yield return new WaitForEndOfFrame();
        }     
    }

    public static void LoadSceneNoLoading(string NameScene)
    {
        System.Action a =()=>
        {
            SceneManager.LoadScene(NameScene);
        };
        Loader.fadeOut(a);
    }
    public static void LoadSceneWithLoading(string NameScene)
    {
        System.Action a =()=>
        {
            SceneManager.LoadSceneAsync("LoadingScene");
            LoadingAnimScript.setAsyncOperator(NameScene, SceneManager.LoadSceneAsync(NameScene, LoadSceneMode.Additive));
        };
        Loader.fadeOut(a);
    }
}
