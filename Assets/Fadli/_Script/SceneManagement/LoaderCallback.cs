﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoaderCallback : MonoBehaviour
{
    private bool isFirstUpdate = true;
    [SerializeField] Text progress;
    [SerializeField] Image background;
    [SerializeField] Slider slider;
    [SerializeField] GameObject rotAnimObject; 
    [SerializeField] float rotSpeed = 1f;
    
    void Start()
    {
        //progress.text = 0+"%";
        
        bg();
        rotate();
    }
    void rotate()
    {
        
        LeanTween.rotateAroundLocal(rotAnimObject, Vector3.forward, 360, rotSpeed).setOnComplete(rotate);
    }
    void Update()
    {
        if(isFirstUpdate)
        {
            Loader.OnLoaderCallback();
            isFirstUpdate = false;
            return;
        }
        //float val = Loader.async.progress;
        //progress.text = (val*100).ToString()+"%";
        //slider.value = val;
    }


    void bg()
    {
        Color color = background.color;
        color.a=0;
        LeanTween.value(background.gameObject, 0f, 0.5f, 0.25f).setOnUpdate((float val)=>
        {
            color.a = val;
            background.color = color;
        });
    }
}
