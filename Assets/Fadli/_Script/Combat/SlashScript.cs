﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlashScript : MonoBehaviour
{
    [SerializeField] CharacterState state;
    public ParticleSystem sparkleFX;
    public ParticleSystem bloodFX;

    [SerializeField] Transform hitPosition;
    [SerializeField] int damage;

    [SerializeField] UnityEvent SlashEvent;

    public void playVFX()
    {
        
        RaycastHit hit;
        if(SlashEvent!=null)
        {
            SlashEvent.Invoke();
        }
        if(Physics.Raycast(hitPosition.position, state.transform.forward, out hit, 1f))
        {
            bool isPlayer = hit.collider.tag == "Player";
            bool isEnemy = hit.collider.tag == "Enemy";
            bool isNPC = hit.collider.tag == "NPC";
            if(isPlayer || isEnemy || isNPC)
            {
                //FX
                bloodFX.transform.position = hit.point;
                bloodFX.Play();
                //Calculate Damage
                //hit.collider.GetComponent<CharacterState>().Damage(damage);
                if (isPlayer)
                {
                    if (transform.parent.tag != "Enemy")
                        hit.collider.GetComponent<PlayerNetworkController>().BroadcastDamageRPC(damage);
                    else
                        if (PhotonNetwork.IsMasterClient)
                            hit.collider.GetComponent<PlayerNetworkController>().BroadcastDamageRPC(damage);
                }
                //Set behaviour
                if(isEnemy)
                {
                    hit.collider.GetComponentInChildren<EnemyBehav>().SetTarget(state.gameObject);
                    hit.collider.GetComponent<EnemyNetworkController>().BrodacastDamageRPC(damage);
                }
            }
            else
            {
                sparkleFX.transform.position = hit.point;
                sparkleFX.Play();
            }

        }
    }

    void targetsurounding()
    {
        
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
