﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShooterScript : MonoBehaviour
{
    [SerializeField] CharacterState state;
    public ParticleSystem gunFX;
    public ParticleSystem sparkleFX;
    public ParticleSystem bloodFX;

    [SerializeField] Transform weaponTip;

    [Header("Setting")]
    [SerializeField] int damage = 2;

    [SerializeField] UnityEvent shootEvent;

    public void playVFX()
    {
        gunFX.Play();
        if(shootEvent!=null)
        {
            shootEvent.Invoke();
        }

        RaycastHit hit;
        if(Physics.Raycast(weaponTip.position - weaponTip.forward*0.5f, state.transform.forward, out hit, 10f))
        {
            bool isPlayer = hit.collider.tag == "Player";
            bool isEnemy = hit.collider.tag == "Enemy";
            bool isNPC = hit.collider.tag == "NPC";
            if(isPlayer|| isEnemy || isNPC)
            {
                bloodFX.transform.position = hit.point;
                bloodFX.Play();
                //hit.collider.GetComponent<CharacterState>().Damage(damage);
                if (isPlayer)
                {
                    if(transform.parent.tag != "Enemy")
                        hit.collider.GetComponent<PlayerNetworkController>().BroadcastDamageRPC(damage);
                    else if(PhotonNetwork.IsMasterClient)
                            hit.collider.GetComponent<PlayerNetworkController>().BroadcastDamageRPC(damage);
                }
                if (isEnemy)
                {
                    if(PhotonNetwork.IsMasterClient && state.isUseNetwork)
                    {
                        hit.collider.GetComponent<EnemyNetworkController>().BrodacastDamageRPC(damage);
                        hit.collider.GetComponent<EnemyNetworkController>().BroadcastSetTarget(state.gameObject);
                        //hit.collider.GetComponentInChi  
                        //hit.collider.GetComponentInChildren<EnemyBehav>().SetTarget(state.gameObject);
                    }                    
                }
            }
            else
            {
                sparkleFX.transform.position = hit.point;
                sparkleFX.Play();
            }

        }
    }

    float delay = 0.2f;
    float currentT =0.2f;
    public void playParticleOnly()
    {
        if(currentT<=0)
        {
            gunFX.Play();
            AudioController.Play("Shoot", this.transform);
            currentT = delay;
        }
        else
        {
            //currentT-=Time.deltaTime*60;
            currentT-=Time.deltaTime;
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
